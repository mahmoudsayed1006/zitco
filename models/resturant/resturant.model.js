import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const RestaurantSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    owner:{
        type:Number,
        ref:'user'
    },
    restaurantName:{
        type:String,
        required: true
    },
    restaurantName_ar:{
        type:String,
        default:""
        //required: true
    },
    title:{
        type:String,
        required: true,
        default:""

    },
    email: {
        type: String,
        default:""
    },
    username: {
        type: String,
    },
    average:{
        type:String,
        required: true
    },
    price:{
        type:Number,
    },
    price2:{
        type:Number,
    },
    space:{
        type:String,
        required: true
    },
    oilType:{
        type:String,
        required: true
    },
    payType:{
        type:String,
        required: true,
        default:""
    },
    address:{
        type:String,
        //required: true
    },
    branchesNumber:{
        type:Number,
        required: true
    },
    img: {
        type: String,
        default:""
    },
    firstname: {
        type: String,
        //required: true,
        trim: true
    },
    lastname: {
        type: String,
        //required: true,
        trim: true
    },
    phone: {
        type:String,
        //required: true,
        trim: true,
        default:""
    },
    note: {
        type:String,
        default:""
    },
    outletPhoneNumber: { //serial for resturant
        type:String,
    },
    outletApprovedSerial: { //serial for resturant approved
        type:Number,
        default:1
    },
    status: {
        type: String,
        enum: ['SURVEY-ACCOUNT','RESTURANT-ACCOUNT','PURCHASING-ACCOUNT','PROCEED', 'APPROVED'],
        default:'SURVEY-ACCOUNT'
    },
    class:{
        type: String,
        enum: ['CLASS-A','CLASS-B','CLASS-C'],
        //default:'CLASS-A'
    },
    Agreement:{
        type: String,
        enum: ['PENDING','AGREEMENT','NON-AGREEMENT'],
        default:'PENDING'
    },
    agreementDate: {
        type: Number,
        default: Date.parse(new Date())
    },
    agreementBy:{
        type:Number,
        ref:'user'
    },
    
    contractDuration:{
        type: Number,
        default:3
    },
    branches: [
        new Schema({
            destination: {
                type: [Number],
                required: true,
            },
            address: {
                type: String,
                //required: true,
            },
            area: {
                type: Number,
                ref:'area',
                required: true,
            },
            city: {
                type: Number,
                ref:'city'
                //required: true,
            },
        }, { _id: false })
        
    ],
    distance:{
        type:Number,
    },
    director:{
        type:Number,
        ref:'user'
    },
    survey:{
        type:Number,
        ref:'user'
    },
    surveyDate: {
        type: Number,
        default: Date.parse(new Date())
    },
    purchasing:{
        type:Number,
        ref:'user'
    }, 
    proceed:{
        type:Number,
        ref:'user'
    },
    operation:{
        type:Number,
        ref:'user'
    },
    approvedBy:{
        type:Number,
        ref:'user'
    },
    approvedDate: {
        type: Number,
        default: Date.parse(new Date())
    },
    decline: {
        type: Boolean,
        default: false
    },
    declineReason:{
        type:String,
    },
    noOil: { //معندوش زيت
        type: Boolean,
        default: true
    },
    uncommitted: {//بقاله شهر او اكثر من غير اوردرات
        type: Boolean,
        default: false
    },
    noOrder: {//المطعم قال للكول سنتر مفيش اوردرات
        type: Boolean,
        default: false
    },
    noOrderReason:{
        type:String,
    },
    noOrderEventUser:{
        type:Number,
        ref:'user'
    },
    reassign:{
        type:Boolean,
        default:false
    },
    nonAgreementReason:{
        type:String,
    },
    lastOrder: {
        type: Number,
    },
    createdDate: {
        type: Number,
        default: Date.parse(new Date())
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

RestaurantSchema.index({ location: '2dsphere' });
RestaurantSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
       
    }
});
autoIncrement.initialize(mongoose.connection);
RestaurantSchema.plugin(autoIncrement.plugin, { model: 'restaurant', startAt: 1 });
RestaurantSchema.plugin(autoIncrement.plugin, { model: 'restaurant', field: 'outletCode', startAt: 10001  });

export default mongoose.model('restaurant', RestaurantSchema);