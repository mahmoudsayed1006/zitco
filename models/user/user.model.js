import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    restaurant:{
        type:Number,
        ref:'restaurant'
    },
    warehouse:{
        type:Number,
        ref:'warehouse'
    },
    firstname: {
        type: String,
        //required: true,
        trim: true
    },
    lastname: {
        type: String,
        //required: true,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        default:"",
        //required: true,
       
    },
    username: {
        type: String,
        trim: true,
        required: true,
        unique: true 
    },
    phone: {
        type:String,
        required: true,
        trim: true,
        default:""
    },
    password: {
        type: String,
        required: true
    },
    job: {
        type: String,
    },
    title: {
        type: String,
    },
    type: {
        type: String,
        enum: ['DRIVER', 'ADMIN','USER','OPERATION','PURCHASING','SURVEY','WAREHOUSE','CALL-CENTER'],
        required:true
    },
    city: {
        type: Number,
        ref: 'city',
        default:1
    },
    area: {
        type: Number,
        ref: 'area',
        default:1
    },
    block:{
        type: Boolean,
        default: false
    },
    location:{
        type:[Number]
    },
    active:{
        type: Boolean,
        default: true
    },
    img: {
        type: String,
        default:""
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    end:{
        type: Boolean,
        default: true
    },
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });

export default mongoose.model('user', userSchema);