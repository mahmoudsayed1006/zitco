import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const warehouseSchema = new Schema({
    _id: {
        type: Number,
        required: true
    }, 
    name: {
        type: String,
        required: true,
        trim: true,
    },
    city: {
        type: Number,
        ref: 'city',
        default:1
    },
    area: {
        type: Number,
        ref: 'area',
        default:1
    },
    capacity: {
        type: Number,
        //required: true,
    },
    freeCapacity: {
        type: Number,
        //required: true,
    },
    oilQuantity:{
        type: Number,
        default:0
    },
    exportOli: {//طلع زيوت قد ايه
        type: Number,
        default:0
    },
    receiveOli: { // جاله زيوت قد ايه
        type: Number,
        default:0
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

warehouseSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



warehouseSchema.plugin(autoIncrement.plugin, { model: 'warehouse', startAt: 1 });

export default mongoose.model('warehouse', warehouseSchema);
