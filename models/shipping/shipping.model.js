import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const shippingSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    driverName: {
        type: String,
        required: true,
    },
    vehicleNumber: {
        type: String,
        required: true,
    },
    waybill: {
        type: String,
        required: true,
    },
    shippingDay: {
        type: String,
        required: true,
        trim: true,
    },
    tankNumber: {
        type: String,
        required: true,
        trim: true,
    },
    warehouse: {
        type: Number,
        ref:'warehouse'
    },
    amount: {//shipping amounts out from warehouse
        type: Number,
        required: true,
    },
    firstMeasure: {//airport measurement
        type: Number,
    },
    secondMeasure: {//client measurement
        type: Number,
    },
    director: {
        type: Number,
        ref:'user'
    },
    accept:{
        type:Boolean,
        default:false
    },
    acceptDate:{
        type:Date
    },
    done:{
        type:Boolean,
        default:false
    },
    doneDate:{
        type:Date
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

shippingSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



shippingSchema.plugin(autoIncrement.plugin, { model: 'shipping', startAt: 1 });

export default mongoose.model('shipping', shippingSchema);
