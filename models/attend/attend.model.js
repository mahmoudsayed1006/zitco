import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";
const AttendSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user:{
        type:Number,
        ref:'user'
    },
    statDate: {
        type: Number,
    },
    start:{
        type:Date,
    },
    endDate: {
        type: Number,
    },
    end:{
        type:Date,
    },
    leave:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

AttendSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
AttendSchema.plugin(autoIncrement.plugin, { model: 'attend', startAt: 1 });

export default mongoose.model('attend', AttendSchema);