import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const OrderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    didUser: {//user add the order
        type: Number,
        ref: 'user',
    },
    restaurant: {
        type: Number,
        ref: 'restaurant',
        required:true
    },
    driver: {
        type: Number,
        ref: 'user',
    },
    wareHouse: {
        type: Number,
        ref: 'warehouse',
    },
    warehouseUser:{//user in warehouse recieve the order
        type: Number,
        ref: 'user',
    },
    city: {
        type: Number,
        ref: 'city',
        default:1
    },
    area: {
        type: Number,
        ref: 'area',
        default:25
    },
    status:{
        type: String,
        enum: ['PENDING','CANCEL', 'ASSIGNED','ONPROGRESS','COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK'],
        required:true,
        default:'PENDING'
    },
    type:{
        type: String,
        enum: ['MANUAL','OTOMATIC'],
        required:true,
        default:'MANUAL'
    },
    average:{//order kg
        type:Number,
        required: true
    },
    date:{
        type:String,
        required: true
    },
    dateInMillesec:{
        type:Number,
    },
    branch:{
        type:String
    },
    reason:{
        type:String
    },
    problem:{//order not done
        type:Boolean,
        default:false
    },
    priceForKg:{
        type:Number,
        default:0
    },
    totalPrice:{
        type:Number,
        default:0
    },
    kg:{ //driver kg
        type:Number,
        default:0
    },
    warehouseKgAmount:{ //warehouse kg الكميه الى استلمها الوير هاوس من السواق
        type:Number,
        default:0
    },
    serialCode:{
        type:String,
    },
    asign:{
        type:Boolean,
        default:false
    },
    asignToWareHouse:{
        type:Boolean,
        default:false
    },
    finish:{//order store in warehouse
        type:Boolean,
        default:false
    },
    actionDate:{
        type:Date,
        default:new Date()
    },
    cancelDate:{//وقت الغاء الطلب
        type:Date,
    },
    assignToDriverDate:{//وقت اسناد المهمه للسواق
        type:Date,
    },
    assignToWarehouseDate:{//وقت اسناد المهمه للمخزن
        type:Date,
    },
    driverTakeDate:{//وقت اسناد المهمه للسواق
        type:Date,
    },
    collectedDate:{//وقت استلام السواق
        type:Date,
    },
    driverArriveDate:{//وقت وصول السواق للمخزن 
        type:Date,
    },
    warehouseReceiveDate:{//وقت استلام الوير هاوس من السواق
        type:Date,
    },
    createdDate:{//وقت استلام السواق
        type:Number,
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OrderSchema.index({ location: '2dsphere' });
OrderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
      
    }
});
autoIncrement.initialize(mongoose.connection);
OrderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });


export default mongoose.model('order', OrderSchema);