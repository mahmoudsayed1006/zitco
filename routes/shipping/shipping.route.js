import express from 'express';
import shippingController from '../../controllers/shipping/shipping.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        shippingController.validateshippingBody(),
        shippingController.create
    )
    .get(shippingController.getAll);

router.route('/:shippingId')
    .put(
        requireAuth,
        shippingController.validateshippingBody(true),
        shippingController.update
    )
    .get(requireAuth,shippingController.getById)
    .delete(requireAuth,shippingController.delete);

router.route('/:shippingId/accept')
    .put(
        requireAuth,
        shippingController.accept
    )
router.route('/:shippingId/done')
    .put(
        requireAuth,
        shippingController.done
    )
router.route('/:shippingId/firstMeasure')
    .put(
        requireAuth,
        shippingController.firstMeasure
    )
router.route('/:shippingId/secondMeasure')
    .put(
        requireAuth,
        shippingController.secondMeasure
    )
export default router;