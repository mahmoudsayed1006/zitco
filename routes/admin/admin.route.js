import express from 'express';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/users')
    .get(adminController.getLastUser);

router.route('/orders')
    .get(adminController.getLastOrder);

router.route('/actions')
    .get(adminController.getLastActions);

router.route('/count')
    .get(adminController.count);


router.route('/getOrderDiagram')
    .get(adminController.getOrderDiagram);

///
router.route('/ordersMonthlyGraph')
.get(adminController.ordersMonthlyGraph);
router.route('/restaurantsMonthlyGraph')
.get(adminController.restaurantsMonthlyGraph);
router.route('/wareHouseLiquidityMonthlyGraph')
.get(adminController.wareHouseLiquidityMonthlyGraph);
router.route('/wareHouseExportMonthlyGraph')
.get(adminController.wareHouseExportMonthlyGraph);
router.route('/restaurantsStatusGraph')
.get(adminController.restaurantsStatusGraph);
router.route('/getRestaurantsSummary')
.get(adminController.resturantsSummary);
router.route('/getRestaurants')
.get(adminController.getRestaurants);
router.route('/:restaurantId/getRestaurantInfo')
.get(adminController.getRestaurantInfo);
router.route('/getOrders')
.get(adminController.getOrders);
router.route('/getOrdersSummary')
.get(adminController.ordersSummary);
//


router.route('/restaurantsInfoGraph')
.get(adminController.restaurantsInfoGraph);
router.route('/ordersInfoGraph')
.get(adminController.ordersInfoGraph);
router.route('/salesStatistics')
.get(adminController.salesStatistics);
router.route('/getRestaurantsSales')
.get(adminController.getRestaurantsSales);
router.route('/branchInfoGraph')
.get(adminController.branchInfoGraph);
router.route('/getBranchs')
.get(adminController.getBranchs);
router.route('/staffStatistics')
.get(adminController.staffStatistics);
router.route('/getStaffUsers')
.get(adminController.getStaffUsers);
router.route('/warehouseStatistics')
.get(adminController.warehouseStatistics);
router.route('/getWarehouse')
.get(adminController.getWarehouse);
router.route('/:userId/surveyMonthlyGraph')
.get(adminController.surveyMonthlyGraph);
router.route('/:userId/purchasingMonthlyGraph')
.get(adminController.purchasingMonthlyGraph);
router.route('/:userId/driverMonthlyGraph')
.get(adminController.driverMonthlyGraph);


export default router;
