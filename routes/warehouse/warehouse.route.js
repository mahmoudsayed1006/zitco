import express from 'express';
import warehouseController from '../../controllers/warehouse/warehouse.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        warehouseController.validatewarehouseBody(),
        warehouseController.create
    )
    .get(warehouseController.getAll);

router.route('/:warehouseId')
    .put(
        requireAuth,
        warehouseController.validatewarehouseBody(true),
        warehouseController.update
    )
    .get(requireAuth,warehouseController.getById)
    .delete(requireAuth,warehouseController.delete);

router.route('/get/withPaginated')
    .get(requireAuth,warehouseController.getAllPaginated);

router.route('/:orderId/availableWareHouse')
    .get(requireAuth,warehouseController.availableWarehouse);

    router.route('/:warehouseId/outOil')
    .put(
        requireAuth,
        warehouseController.outOil
    )

export default router;