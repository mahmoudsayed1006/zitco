import express from 'express';
import SliderController from '../../controllers/slider/slider.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
 
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('Slider').array('img',10),
        SliderController.validateBody(),
        SliderController.create
    )
    .get(SliderController.getAll);
    
router.route('/:SliderId')
    .put(
        requireAuth,
        multerSaveTo('Slider').fields([
            { name: 'img', maxCount: 10, options: false }
        ]),
        SliderController.validateBody(true),
        SliderController.update
    )
    .get(SliderController.findById)
    .delete( requireAuth,SliderController.delete);
    
router.route('/:SliderId/deleteImg')
    .put(
        requireAuth,
        SliderController.deleteImg
    )
router.route('/:SliderId/addImg')
    .put(
        multerSaveTo('Slider').single('img'),
        requireAuth,
        SliderController.addImg
    )
router.route('/:SliderId/updateSlider')
    .put(
        requireAuth,
        SliderController.updateSlider
    )
export default router;