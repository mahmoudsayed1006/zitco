import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/find')
    .get(UserController.findAll);
    
router.route('/:id/getUser')
    .get(UserController.findById);

router.route('/getUsers')
    .get(UserController.findAllWithoutPagenation);
router.route('/getAttends')
    .get(UserController.findAttend);
router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete);

router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );
router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );
router.route('/:userId/tracking')
    .put(
        requireAuth,
        UserController.tracking
    );

router.route('/:userId/start')
    .put(
        requireAuth,
        UserController.start
    );
router.route('/:userId/end')
    .put(
        requireAuth,
        UserController.end
    );

router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);


router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedUser(true),
    UserController.updateAdminInfo);

router.put('/user/:userId/updateAdminInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedUser(true),
    UserController.updateAdminInfo);

router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

export default router;
