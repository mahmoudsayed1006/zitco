import express from 'express';
import ResturantController from '../../controllers/resturant/resturant.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();

router.route('/accountCount')
    .get(ResturantController.countAccount)

router.route('/:surveyId/surveyAccountCount')
    .get(ResturantController.getAccountCountBySurvey)

router.route('/:purchasingId/purchasingAccountCount')
    .get(ResturantController.getAccountCountByPurchasing)

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('Resturant').single('img'),
        parseStringToArrayOfObjectsMw('branches'),
        ResturantController.validateCreatedrestaurants(),
        ResturantController.create
    )
    .get(ResturantController.findrestaurants)
router.route('/signUpResturant')
    .post(
        multerSaveTo('Resturant').single('img'),
        parseStringToArrayOfObjectsMw('branches'),
        ResturantController.validateCreatedrestaurants(),
        ResturantController.signUp
    )
router.route('/:ResturantId')
    .put(
        requireAuth,
        multerSaveTo('Resturant').single('img'),
        parseStringToArrayOfObjectsMw('branches'),
        ResturantController.validateCreatedrestaurants(true),
        ResturantController.update
    )
    .get(ResturantController.findById)
    .delete( requireAuth,ResturantController.delete);

router.route('/:ResturantId/purchasing')
    .put(
        requireAuth,
        ResturantController.PURCHASING
    )
router.route('/:ResturantId/purchasing/:userId')
    .put(
        requireAuth,
        ResturantController.PURCHASINGChosen
    )
router.route('/:ResturantId/proceed')
    .put(
        requireAuth,
        ResturantController.PROCEED
    )

router.route('/:ResturantId/approved')
    .put(
        requireAuth,
        ResturantController.APPROVED
    )
router.route('/:ResturantId/ignore')
    .put(
        requireAuth,
        ResturantController.ignore
    )
router.route('/:ResturantId/removeIgnore')
    .put(
        requireAuth,
        ResturantController.removeIgnore
    )

router.route('/:ResturantId/agreement')
    .put(
        requireAuth,
        ResturantController.agreement
    )
router.route('/:ResturantId/nonAgreement')
    .put(
        requireAuth,
        ResturantController.nonAgreement
    )

router.route('/:ResturantId/noOrder')
    .put(
        requireAuth,
        ResturantController.noOrder
    )


export default router;
