import express from 'express';
import userRoute from './user/user.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AboutRoute  from './about/about.route';
import AdminRoute  from './admin/admin.route';
import NewsRoute  from './news/news.route';
import SliderRoute  from './slider/slider.route';
import RestaurantRoute  from './resturant/resturant.route';
import OrderRoute  from './order/order.route';
import CityRoute  from './city/city.route';
import WarehouseRoute  from './warehouse/warehouse.route';
import ShippingRoute from './shipping/shipping.route'
import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/slider',SliderRoute);
router.use('/news',NewsRoute);
router.use('/restaurant',RestaurantRoute);
router.use('/orders',OrderRoute);
router.use('/cities',CityRoute);
router.use('/warehouses',WarehouseRoute);
router.use('/shipping',ShippingRoute);
export default router;
