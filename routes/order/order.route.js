import express from 'express';
import OrderController from '../../controllers/order/order.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .get(OrderController.findOrders)

router.route('/orderCount')
    .get(OrderController.countOrder)

router.route('/getOrder')
    .get(OrderController.getOrder)

router.route('/getOliAverageAndCount')
    .get(OrderController.getOliAverageAndCount)

router.route('/:orderId')
    .put( requireAuth,OrderController.update) 
    .get(OrderController.findById)
    .delete( requireAuth,OrderController.delete);

router.route('/:orderId/cancel')
    .put( requireAuth,OrderController.cancel) 

router.route('/:orderId/accept')
    .put( requireAuth,OrderController.accept)

router.route('/:orderId/finish')
    .put( requireAuth,OrderController.collect)

router.route('/:orderId/problem')
    .put( requireAuth,OrderController.problem)

router.route('/:orderId/arrive')
    .put( requireAuth,OrderController.arrive)   

router.route('/:orderId/stored')
    .put( requireAuth,OrderController.stored)  
export default router;
