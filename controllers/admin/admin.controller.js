import ApiError from "../../helpers/ApiError";
import Restaurant from "../../models/resturant/resturant.model";
import User from "../../models/user/user.model";
import Order from "../../models/order/order.model";
import Warehouse from "../../models/warehouse/warehouse.model";
import Report from "../../models/reports/report.model";
import Message from "../../models/contact/contact.model";
import City from "../../models/city/city.model";
import Area from "../../models/area/area.model";
import Shipping from "../../models/shipping/shipping.model";
import ApiResponse from "../../helpers/ApiResponse";

import moment from 'moment';
const populateQuery = [
    { path: 'restaurant', model: 'restaurant' },
];
const action = [
    { path: 'user', model: 'user' },
]
const populateQueryrestaurant = [
    { path: 'operation', model: 'user' },
    { path: 'owner', model: 'user' },
    { path: 'approvedBy', model: 'user' },
    { path:'agrementActionBy', model: 'user'},
    { path:'survey', model: 'user'},
    { path: 'branches.city', model: 'city' },
    { path: 'branches.area', model: 'area' },
];
const populateQueryOrder = [
    { path: 'restaurant', model: 'restaurant' },
    { path:'didUser', model: 'user'},
    { path: 'driver', model: 'user' },
    { path: 'city', model: 'city' },
    { path: 'area', model: 'area' },
];

let startM1 = moment(new Date()).add(0, 'M').startOf('month').format('YYYY-MM-DD')
var m1End =  moment(new Date()).add(0, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth1 = startM1 +'T00:00:00.000Z'
let endMonth1 = m1End +'T23:59:00.000Z'
///////////////////////////////////////////////////////////
let startM2 = moment(new Date()).add(-1, 'M').startOf('month').format('YYYY-MM-DD')
var m2End = moment(new Date()).add(-1, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth2 = startM2 +'T00:00:00.000Z'
let endMonth2 = m2End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM3 = moment(new Date()).add(-2, 'M').startOf('month').format('YYYY-MM-DD')
var m3End =  moment(new Date()).add(-2, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth3 = startM3 +'T00:00:00.000Z'
let endMonth3 = m3End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM4 = moment(new Date()).add(-3, 'M').startOf('month').format('YYYY-MM-DD')
var m4End = moment(new Date()).add(-3, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth4 = startM4 +'T00:00:00.000Z'
let endMonth4 = m4End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM5 = moment(new Date()).add(-4, 'M').startOf('month').format('YYYY-MM-DD')
var m5End =  moment(new Date()).add(-4, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth5 = startM5 +'T00:00:00.000Z'
let endMonth5 = m5End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM6 = moment(new Date()).add(-5, 'M').startOf('month').format('YYYY-MM-DD')
var m6End =  moment(new Date()).add(-5, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth6 = startM6 +'T00:00:00.000Z'
let endMonth6 = m6End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM7 = moment(new Date()).add(-6, 'M').startOf('month').format('YYYY-MM-DD')
var m7End =  moment(new Date()).add(-6, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth7 = startM7 +'T00:00:00.000Z'
let endMonth7 = m7End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM8 = moment(new Date()).add(-7, 'M').startOf('month').format('YYYY-MM-DD')
var m8End =  moment(new Date()).add(-7, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth8 = startM8 +'T00:00:00.000Z'
let endMonth8 = m8End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM9 = moment(new Date()).add(-8, 'M').startOf('month').format('YYYY-MM-DD')
var m9End =  moment(new Date()).add(-8, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth9 = startM9 +'T00:00:00.000Z'
let endMonth9 = m9End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM10 = moment(new Date()).add(-9, 'M').startOf('month').format('YYYY-MM-DD')
var m10End =  moment(new Date()).add(-9, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth10 = startM10 +'T00:00:00.000Z'
let endMonth10 = m10End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM11 = moment(new Date()).add(-10, 'M').startOf('month').format('YYYY-MM-DD')
var m11End =  moment(new Date()).add(-19, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth11 = startM11 +'T00:00:00.000Z'
let endMonth11 = m11End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////
let startM12 = moment(new Date()).add(-11, 'M').startOf('month').format('YYYY-MM-DD')
var m12End = moment(new Date()).add(-11, 'M').endOf('month').format('YYYY-MM-DD');
let startMonth12 = startM12 +'T00:00:00.000Z'
let endMonth12 = m12End +'T23:59:00.000Z'
////////////////////////////////////////////////////////////////////////////
//today
let today = moment(new Date()).format('YYYY-MM-DD')
let startDay = today +'T00:00:00.000Z'
let endDay = today +'T23:59:00.000Z'
console.log(startDay)
console.log(endDay)
//this week
let startWeek = moment(new Date()).startOf('week').add(-1, 'd').format('YYYY-MM-DD')
var endWeek =  moment(new Date()).endOf('week').add(-1, 'd').format('YYYY-MM-DD');
startWeek = startWeek +'T00:00:00.000Z'
endWeek = endWeek +'T23:59:00.000Z'
console.log(startWeek)
console.log(endWeek)
//this month
let startMonth = moment(new Date()).startOf('month').format('YYYY-MM-DD')
var endMonth = moment(new Date()).endOf('month').format('YYYY-MM-DD')
startMonth = startMonth +'T00:00:00.000Z'
endMonth = endMonth +'T23:59:00.000Z'
console.log(startMonth)
console.log(endMonth)
//this quarter
let startQuarter = moment(new Date()).add(0, 'M').format('YYYY-MM-DD')
let endQuarter = moment(new Date()).add(-3, 'M').endOf('month').format('YYYY-MM-DD');
startQuarter = startQuarter +'T00:00:00.000Z'
endQuarter = endQuarter +'T23:59:00.000Z'
console.log(startQuarter)
console.log(endQuarter)
//last 6 month
let start6Months = moment(new Date()).add(0, 'M').format('YYYY-MM-DD')
let end6MonthsStart = moment(new Date()).add(-5, 'M').endOf('month').format('YYYY-MM-DD');
let end6Months = moment(end6MonthsStart).format("YYYY-MM-") + moment(end6MonthsStart).daysInMonth();
start6Months = start6Months +'T00:00:00.000Z'
end6Months = end6Months +'T23:59:00.000Z'
console.log(start6Months)
console.log(end6Months)
//this year
let startThisYear = moment(new Date()).startOf('year').format('YYYY-MM-DD')
var endThisYear =  moment(new Date()).endOf('year').format('YYYY-MM-DD');
startThisYear = startThisYear +'T00:00:00.000Z'
endThisYear = endThisYear +'T23:59:00.000Z'
console.log(startThisYear)
console.log(endThisYear)
//last year
let startLastYear = moment(new Date()).startOf('year').add(-1, 'Y').format('YYYY-MM-DD')
let endLastYear = moment(startLastYear).endOf('year').format('YYYY-MM-DD')
startLastYear = startLastYear +'T00:00:00.000Z'
endLastYear = endLastYear +'T23:59:00.000Z'
console.log(startLastYear)
console.log(endLastYear)

export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'USER'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastOrder(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastOrder = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastOrder);
        } catch (error) {
            next(error);
        }
    },
    async getOrderDiagram(req, res, next) {
        try {
            let user = req.user;
            
            let d = new Date().toISOString().slice(0, 5);
            console.log(d) 
            let data =[];
            let query;
            let month = 0;
            for(var j = 1; j <= 12 ;j++){
                if (j>9){
                    month = j
                } else{
                    month = '0' + j
                }
                console.log("i = " , j)
                var firstDayInMonth = d + month +'-01T00:00:00.000Z';
                let firstDayInMonthMilleSec = Date.parse(firstDayInMonth)
                var lastDayInMonth = d + month +'-31T23:59:00.000Z';
                let lastDayInMonthMilleSec = Date.parse(lastDayInMonth)
                console.log(firstDayInMonth)
                console.log(lastDayInMonth)
                query = {
                    $and: [
                        {createdDate: { $gte : firstDayInMonthMilleSec, $lte :lastDayInMonthMilleSec}},
                        {deleted: false }
                    ]
                };            
                let orders = await Order.find(query)
                var total = 0;

                for (var i = 0; i < orders.length; i++) { 
                        total += orders[i].kg
                } 
                data.push(total)
                    
            }
            

            res.send(data);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count({ deleted: false,type:'USER' });
            const DriverCount = await User.count({ deleted: false,type:'DRIVER' });
            const OPERATIONCount = await User.count({ deleted: false,type:'OPERATION' });

            const PURCHASINGCount = await User.count({ deleted: false,type:'PURCHASING' });
            const SURVEYCount = await User.count({ deleted: false,type:'SURVEY' });

            const PendingOrdersCount = await Order.count({deleted:false,status:'PENDING'});
            const ASSIGNEDOrdersCount = await Order.count({deleted:false,status:'ASSIGNED'});
            const RefusedOrdersCount = await Order.count({deleted:false,status:'CANCEL'});

            const COLLECTEDOrdersCount = await Order.count({deleted:false,status:'COLLECTED'});
            const ARRIVEDOrdersCount = await Order.count({deleted:false,status:'ARRIVED'});//
            const STOREDOrdersCount = await Order.count({deleted:false,status:'STORED'});

            const messageCount = await Message.count({deleted:false,reply:false});
            const warehousesCount = await Warehouse.count(query);
            const restaurantsCount = await restaurant.count(query);
            
            const areasCount = await Area.count(query);//
            const citiesCount = await City.count(query);//
            const ShippingCount = await Shipping.count(query);

          
            let total = await Order.find(query);
            var sum = 0;
            for (var i = 0; i < total.length; i++) { 
              sum += total[i].kg
            }

           
            res.status(200).send({
                ShippingCount:ShippingCount,
                areasCount:areasCount,//
                restaurantsCount:restaurantsCount,
                warehousesCount:warehousesCount,
                STOREDOrdersCount:STOREDOrdersCount,
                ARRIVEDOrdersCount:ARRIVEDOrdersCount,//
                COLLECTEDOrdersCount:COLLECTEDOrdersCount,
                RefusedOrdersCount:RefusedOrdersCount,
                ASSIGNEDOrdersCount:ASSIGNEDOrdersCount,
                PendingOrdersCount:PendingOrdersCount,
                SURVEYCount:SURVEYCount,
                PURCHASINGCount:PURCHASINGCount,
                usersCount:usersCount,
                DriverCount:DriverCount,
                OPERATIONCount:OPERATIONCount,
                messages:messageCount,               
                totalSales:sum,
                cities:citiesCount,//
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    ////////////////////////////////////////////////////////////////////////////////////////////////

    async ordersMonthlyGraph(req,res, next) {
        try {
            let orders1 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth1) , $lte : Date.parse(endMonth1) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders2 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth2) , $lte : Date.parse(endMonth2) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders3 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth3) , $lte : Date.parse(endMonth3) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders4 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth4) , $lte : Date.parse(endMonth4) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders5 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth5) , $lte : Date.parse(endMonth5) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders6 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth6) , $lte : Date.parse(endMonth6) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders7 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth7) , $lte : Date.parse(endMonth7) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders8 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth8) , $lte : Date.parse(endMonth8) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders9 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth9) , $lte : Date.parse(endMonth9) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders10 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth10) , $lte : Date.parse(endMonth10) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders11 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth11) , $lte : Date.parse(endMonth11) }
            })
            ////////////////////////////////////////////////////////////////////
            let orders12 = await Order.countDocuments({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth12) , $lte : Date.parse(endMonth12) }
            })
            let orders = [orders1,orders2,orders3,orders4,orders5,orders6,orders7,orders8,orders9,orders10,orders11,orders12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                orders,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async restaurantsMonthlyGraph(req,res, next) {
        try {
            let restaurants1 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants2 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth2, $lte : endMonth2 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants3 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth3, $lte : endMonth3 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants4 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth4, $lte : endMonth4 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants5 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth5, $lte : endMonth5 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants6 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth6, $lte : endMonth6 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants7 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth7, $lte : endMonth7 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants8 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth8, $lte : endMonth8 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants9 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth9, $lte : endMonth9 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants10 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth10, $lte : endMonth10 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants11 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth11, $lte : endMonth11 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants12 = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth12, $lte : endMonth12 }
            })
            let restaurants = [restaurants1,restaurants2,restaurants3,restaurants4,restaurants5,restaurants6,restaurants7,restaurants8,restaurants9,restaurants10,restaurants11,restaurants12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                restaurants,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async wareHouseLiquidityMonthlyGraph(req,res, next) {
        try {
            let orders1 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth1) , $lte : Date.parse(endMonth1) }
            }).select('warehouseKgAmount')
            console.log(orders1)
            orders1 =orders1.length>0? orders1.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
           console.log(orders1)
            ////////////////////////////////////////////////////////////////////
            let orders2 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth2) , $lte : Date.parse(endMonth2) }
            }).select('warehouseKgAmount')
            console.log(orders2)
            orders2 =orders2.length>0? orders2.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders3 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth3) , $lte : Date.parse(endMonth3) }
            }).select('warehouseKgAmount')
            orders3 =orders3.length>0? orders3.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders4 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth4) , $lte : Date.parse(endMonth4) }
            }).select('warehouseKgAmount')
            orders4 =orders4.length>0? orders4.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders5 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth5) , $lte : Date.parse(endMonth5) }
            }).select('warehouseKgAmount')
            orders5 =orders5.length>0? orders5.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders6 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth6) , $lte : Date.parse(endMonth6) }
            }).select('warehouseKgAmount')
            orders6 =orders6.length>0? orders6.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders7 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth7) , $lte : Date.parse(endMonth7) }
            }).select('warehouseKgAmount')
            orders7 =orders7.length>0? orders7.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders8 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth8) , $lte : Date.parse(endMonth8) }
            }).select('warehouseKgAmount')
            orders8 =orders8.length>0? orders8.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders9 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth9) , $lte : Date.parse(endMonth9) }
            }).select('warehouseKgAmount')
            orders9 =orders9.length>0? orders9.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders10 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth10) , $lte : Date.parse(endMonth10) }
            }).select('warehouseKgAmount')
            orders10 = orders10.length>0?orders10.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders11 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth11) , $lte : Date.parse(endMonth11) }
            }).select('warehouseKgAmount')
            orders11 = orders11.length>0?orders11.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders12 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                createdDate:{$gte :Date.parse( startMonth12) , $lte : Date.parse(endMonth12) }
            }).select('warehouseKgAmount')
            orders12 = orders12.length>0?orders12.map(o => o.warehouseKgAmount).reduce((a, c) => { return a + c }):0
            let orders = [orders1,orders2,orders3,orders4,orders5,orders6,orders7,orders8,orders9,orders10,orders11,orders12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                orders,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async wareHouseExportMonthlyGraph(req,res, next) {
        try {
            let startM1 = moment(new Date()).startOf('month').startOf('month').format('YYYY-MM-DD')
            var m1End = moment(startM1).format("YYYY-MM-") + moment(startM1).daysInMonth();
            //end = end +'T23:59:59.000Z'
            let startMonth1 = startM1 +'T00:00:00.000Z'
            let endMonth1 = m1End +'T23:59:00.000Z'
            let shipping1 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth1, $lte : endMonth1 }
            }).select('amount')
            shipping1 = shipping1.length > 0 ? shipping1.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("1")
            ////////////////////////////////////////////////////////////////////
            let shipping2 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth2, $lte : endMonth2 }
            }).select('amount')
            shipping2 = shipping2.length>0?shipping2.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("2")
            ////////////////////////////////////////////////////////////////////
            let shipping3 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth3, $lte : endMonth3 }
            }).select('amount')
            shipping3 = shipping3.length>0?shipping3.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("3")
            ////////////////////////////////////////////////////////////////////
            let shipping4 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth4, $lte : endMonth4 }
            }).select('amount')
            shipping4 = shipping4.length>0?shipping4.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("4")
            ////////////////////////////////////////////////////////////////////
            let shipping5 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth5, $lte : endMonth5 }
            }).select('amount')
            shipping5 = shipping5.length>0?shipping5.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("5")
            ////////////////////////////////////////////////////////////////////
            let shipping6 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth6, $lte : endMonth6 }
            }).select('amount')
            shipping6 = shipping6.length>0?shipping6.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("6")
            ////////////////////////////////////////////////////////////////////
            let shipping7 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth7, $lte : endMonth7 }
            }).select('amount')
            shipping7 = shipping7.length>0?shipping7.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("7")
            ////////////////////////////////////////////////////////////////////
            let shipping8 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth8, $lte : endMonth8 }
            }).select('amount')
            shipping8 = shipping8.length>0?shipping8.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("8")
            ////////////////////////////////////////////////////////////////////
            let shipping9 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth9, $lte : endMonth9 }
            }).select('amount')
            shipping9 = shipping9.length>0?shipping9.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("19")
            ////////////////////////////////////////////////////////////////////
            let shipping10 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth10 , $lte :(endMonth10) }
            }).select('amount')
            shipping10 = shipping10.length>0?hipping10.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("10")
            ////////////////////////////////////////////////////////////////////
            let shipping11 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth11 , $lte :(endMonth11) }
            }).select('amount')
            shipping11 = shipping11.length>0?hipping11.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("11")
            ////////////////////////////////////////////////////////////////////
            let shipping12 = await Shipping.find({
                deleted: false,
                accept:true,
                acceptDate:{$gte :startMonth12 , $lte :(endMonth12) }
            }).select('amount')
            shipping12 = shipping12.length>0?hipping12.map(o => o.amount).reduce((a, c) => { return a + c }):0
            console.log("12")
            let shipping = [shipping1,shipping2,shipping3,shipping4,shipping5,shipping6,shipping7,shipping8,shipping9,shipping10,shipping11,shipping12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                shipping,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async restaurantsStatusGraph(req,res, next) {//done
        try {
            let surveyRestaurants = await Restaurant.countDocuments({
                deleted: false,
                status:'SURVEY-ACCOUNT',
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let pendingRestaurants = await Restaurant.countDocuments({
                deleted: false,
                Agreement:'PENDING',
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let approvedRestaurants = await Restaurant.countDocuments({
                deleted: false,
                status:'APPROVED',
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let activeRestaurants = await Restaurant.countDocuments({
                deleted: false,
                uncommitted:false,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let notActiveRestaurants = await Restaurant.countDocuments({
                deleted: false,
                uncommitted:true,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let declineRestaurants = await Restaurant.countDocuments({
                deleted: false,
                decline:true,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            let totalRestaurants = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            let totalRestaurantsLastMonth = await Restaurant.countDocuments({
                deleted: false,
                createdAt:{$gte :startMonth2, $lte : endMonth2 }
            })
           
            let restaurants = [surveyRestaurants,pendingRestaurants,approvedRestaurants,activeRestaurants,notActiveRestaurants,declineRestaurants];
            res.send({restaurants:restaurants, totalRestaurants:totalRestaurants,totalRestaurantsLastMonth:totalRestaurantsLastMonth})
        } catch (err) {
            next(err);
        }
    },
    async resturantsSummary(req,res, next) {///done

        let {date,dateType}= req.query;
        if(!date && !dateType){
            date = new Date();
            dateType = 'YEAR'
        }
        let resturants = [];
        let dateNames = [] ;
        let year = moment(date,"YYYY-MM-DD").format("YYYY")
        
        if(dateType =="YEAR"){ 
            for (let i = 1; i <= 12; i++) {
                let month = i;
                if(i <= 9){
                    month = "0" + i
                }
                console.log("month", month)
                let startDate = year +"-"+ month + "-01" + 'T00:00:00.000Z';
                let dateName = moment(startDate).format('MMMM')
                dateNames.push(dateName)
                console.log("startDate",startDate)
                //let daysNum = moment(startDate).daysInMonth()
                let endDate =moment(startDate).endOf('month').format('YYYY-MM-DD')+ 'T23:59:00.000Z';
                console.log("endDate",endDate)
                let query = {deleted: false,createdDate:{$gte :Date.parse(startDate), $lte : Date.parse(endDate) }}
                let resturantsCount = await Restaurant.countDocuments(query)
                resturants.push(resturantsCount)
                
            }
            //this quarter
            let startQuarter = moment(date).add(0, 'M').format('YYYY-MM-DD')
            let endQuarter =  moment(date).add(-3, 'M').endOf('month').format('YYYY-MM-DD');
            startQuarter = startQuarter +'T00:00:00.000Z'
            endQuarter = endQuarter +'T23:59:00.000Z'
            let dateName1 = 'Quarter'
            dateNames.push(dateName1)
            //this year
            let startThisYear = moment(date).startOf('year').format('YYYY-MM-DD')
            var endThisYear =  moment(date).endOf('year').format('YYYY-MM-DD');
            startThisYear = startThisYear +'T00:00:00.000Z'
            endThisYear = endThisYear +'T23:59:00.000Z'
            let dateName2 = 'ThisYear'
            dateNames.push(dateName2)
            //last year
            let startLastYear = moment(date).startOf('year').add(-1, 'Y').format('YYYY-MM-DD')
            let endLastYear = moment(startLastYear).endOf('year').format('YYYY-MM-DD')
            startLastYear = startLastYear +'T00:00:00.000Z'
            endLastYear = endLastYear +'T23:59:00.000Z'
            let dateName3 = 'LastYear'
            dateNames.push(dateName3)
            let quarterResturantsCount = await Restaurant.countDocuments({
                deleted: false,
                createdDate:{$gte :Date.parse(startQuarter), $lte:Date.parse( endQuarter) }
            })
            
            resturants.push(quarterResturantsCount)
            let thisYearResturantsCount = await Restaurant.countDocuments({
                deleted: false,
                createdDate:{$gte :Date.parse(startThisYear), $lte: Date.parse(endThisYear) }
            })
            resturants.push(thisYearResturantsCount)
            let lastYearResturantsCount = await Restaurant.countDocuments({
                deleted: false,
                createdDate:{$gte :Date.parse(startLastYear), $lte: Date.parse(endLastYear )}
            })
            resturants.push(lastYearResturantsCount)
            
        }else{
            let dayInMonth =moment(date).daysInMonth();
            let month = moment(date,"YYYY-MM-DD").format("MM")
            for (let i = 1; i <= dayInMonth; i++) {
                let day = i;
                if(i <= 9){
                    day = "0" + i
                }
                
                let startDate = year +"-"+ month +"-"+day + 'T00:00:00.000Z';
                console.log("startDate",startDate)
                //let dateName = moment(startDate).format('MMMM')
                dateNames.push(startDate)
                let endDate = moment(startDate).endOf('month').format('YYYY-MM-DD')+ 'T23:59:00.000Z';
                console.log("endDate",endDate)
                let query = {deleted: false,createdDate:{$gte :Date.parse(startDate), $lte : Date.parse(endDate) }}
                let resturantsCount = await Restaurant.countDocuments(query)
                resturants.push(resturantsCount)
                
            }
            //this month
            let startThisMonth = moment(date).startOf('month').format('YYYY-MM-DD')
            var endThisMonth =  moment(date).endOf('month').format('YYYY-MM-DD');
            startThisMonth = startThisMonth +'T00:00:00.000Z'
            endThisMonth = endThisMonth +'T23:59:00.000Z'
            let dateName2 = 'ThisMonth'
            dateNames.push(dateName2)
            let resturantsThisMonthCount = await Restaurant.countDocuments({
                deleted: false,
                createdDate:{$gte :Date.parse(startThisMonth), $lte:Date.parse(endThisMonth) }
            })
            resturants.push(resturantsThisMonthCount)
        }
        res.send({
            resturants:resturants,
            dateNames:dateNames
        })
        
    },
    async getRestaurants(req,res, next) {//done 
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,status,uncommitted,city,date,dateType} = req.query;
        let query = {deleted: false};
        if(area){
            Object.assign(query, {"branches.area": area});
        } 
        if(city){
            Object.assign(query, {"branches.city": city});
        } 
        if (status) query.status = status;
        if(date && dateType){  
            let startDate,endDate
            if(dateType =="DAY"){
                startDate = date + 'T00:00:00.000Z';
                endDate = date + 'T23:59:00.000Z';
                
            }else{
                startDate = moment(date).add(0, 'M').startOf('month').format('YYYY-MM-DD')
                endDate = moment(startDate).format("YYYY-MM-") + moment(startDate).daysInMonth();
                startDate = startDate +'T00:00:00.000Z'
                endDate = endDate +'T23:59:00.000Z'
            }
            Object.assign(query, {"createdAt": {$gte :startDate, $lte : endDate }});
            
        }
        if (uncommitted == "true") query.uncommitted = true;
        if (uncommitted =="false") query.uncommitted = false;
        
        Restaurant.find(query)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .populate(populateQueryrestaurant).then(async(data)=>{ 
            let newdata = []
            await Promise.all(data.map(async(e)=>{
                let lastCollectedDate = await Order.findOne({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                .sort({collectedDate: -1}).select('createdAt')
                let totalCollected = await Order.find({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                .select('kg')
                totalCollected = totalCollected.length>0?totalCollected.map(o => o.kg).reduce((a, c) => { return a + c }):0
                let index = {
                    restaurantName_en:e.restaurantName,
                    restaurantName_ar:e.restaurantName_ar,
                    status:e.status,
                    oilType:e.oilType,
                    payType:e.payType,
                    space:e.space,
                    Agreement:e.Agreement,
                    agreementDate:moment(e.agreementDate).format(),
                    contractDuration:e.contractDuration,
                    phone:e.phone,
                    outletPhoneNumber:e.outletPhoneNumber,
                    surveyDate:moment(e.surveyDate).format(),
                    approvedDate:moment(e.approvedDate).format(),
                    estimatedQuantity:e.average,
                    price:e.price,
                    lastCollectedDate:lastCollectedDate?lastCollectedDate.createdAt:"",
                    totalCollected:totalCollected,
                    createdDate:moment(e.createdDate).format(),
                    id:e._id,
                }
                if(e.survey){
                    index.survey ={
                        firstname:e.survey.firstname,
                        lastname:e.survey.lastname,
                        username:e.survey.username,
                        id:e.survey._id
                    }
                }
                if(e.owner){
                    index.owner ={
                        firstname:e.owner.firstname,
                        lastname:e.owner.lastname,
                        username:e.owner.username,
                        phone:e.owner.phone,
                        title:e.owner.title,
                        id:e.owner._id
                    }
                }
                if(e.approvedBy){
                    index.approvedBy ={
                        firstname:e.approvedBy.firstname,
                        lastname:e.approvedBy.lastname,
                        username:e.approvedBy.username,
                        id:e.approvedBy._id
                    }
                }
                /*branches */
                let branches = []
                for (let val of e.branches) {
                    branches.push({
                        city:{ 
                            cityName:val.city.cityName,
                            arabicCityName:val.city.arabicCityName,
                            id: val.city._id,
                        },
                        area:{ 
                            areaName:val.area.areaName,
                            arabicAreaName:val.area.arabicAreaName,
                            id: val.area._id,
                        },
                        destination:val.destination,
                        address:val.address,
                    })
                }
                index.branches = branches;
                newdata.push(index)
            }))
            console.log(newdata)
            const count = await Restaurant.countDocuments(query);
            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
        })
    }, 
    async getRestaurantInfo(req,res, next) {//done 
        let {restaurantId} = req.params;
        Restaurant.findById(restaurantId)
        .populate(populateQueryrestaurant).then(async(e)=>{ 
            let lastCollectedDate = await Order.findOne({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                .sort({collectedDate: -1}).populate([{path:'driver', model: 'user'}])
            let totalCollected = await Order.find({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
            .select('kg')
            totalCollected = totalCollected.length>0?totalCollected.map(o => o.kg).reduce((a, c) => { return a + c }):0
            let index = {
                restaurantName_en:e.restaurantName,
                restaurantName_ar:e.restaurantName_ar,
                status:e.status,
                oilType:e.oilType,
                payType:e.payType,
                space:e.space,
                Agreement:e.Agreement,
                agreementDate:moment(e.agreementDate).format(),
                agreementStartDate:moment(e.agreementDate).format(),
                agreementEndDate:moment(e.agreementDate).format(),
                contractDuration:e.contractDuration,
                phone:e.phone,
                outletPhoneNumber:e.outletPhoneNumber,
                surveyDate:moment(e.surveyDate).format(),
                approvedDate:moment(e.approvedDate).format(),
                estimatedQuantity:e.average,
                price:e.price,
                lastCollectedDate:lastCollectedDate?lastCollectedDate.createdAt:"",
                totalCollected:totalCollected,
                createdDate:moment(e.createdDate).format(),
                id:e._id,
            }
            if(lastCollectedDate){
                index.driver ={
                    firstname:lastCollectedDate.driver.firstname,
                    lastname:lastCollectedDate.driver.lastname,
                    username:lastCollectedDate.driver.username,
                    id:lastCollectedDate.driver._id
                }
            }
            /*branches */
            let branches = []
            for (let val of e.branches) {
                branches.push({
                    city:{ 
                        cityName:val.city.cityName,
                        arabicCityName:val.city.arabicCityName,
                        id: val.city._id,
                    },
                    area:{ 
                        areaName:val.area.areaName,
                        arabicAreaName:val.area.arabicAreaName,
                        id: val.area._id,
                    },
                    destination:val.destination,
                    address:val.address,
                })
            }
            if(e.survey){
                index.survey ={
                    firstname:e.survey.firstname,
                    lastname:e.survey.lastname,
                    username:e.survey.username,
                    id:e.survey._id
                }
            }
            if(e.owner){
                index.owner ={
                    firstname:e.owner.firstname,
                    lastname:e.owner.lastname,
                    username:e.owner.username,
                    phone:e.owner.phone,
                    title:e.owner.title,
                    id:e.owner._id
                }
            }
            if(e.approvedBy){
                index.approvedBy ={
                    firstname:e.approvedBy.firstname,
                    lastname:e.approvedBy.lastname,
                    username:e.approvedBy.username,
                    id:e.approvedBy._id
                }
            }
            if(e.agrementActionBy){
                index.agrementActionBy ={
                    firstname:e.agrementActionBy.firstname,
                    lastname:e.agrementActionBy.lastname,
                    username:e.agrementActionBy.username,
                    id:e.agrementActionBy._id
                }
            }
            
            index.branches = branches;
            res.send({resturant:index});
        })
    }, 
    async ordersSummary(req,res, next) {///done

        let {date,dateType}= req.query;
        if(!date && !dateType){
            date = new Date();
            dateType = 'YEAR'
        }
        let orders = [];
        let dateNames = [] ;
        let year = moment(date,"YYYY-MM-DD").format("YYYY")
        
        if(dateType =="YEAR"){ 
            for (let i = 1; i <= 12; i++) {
                let month = i;
                if(i <= 9){
                    month = "0" + i
                }
                
                let startDate = year +"-"+ month + "-01" + 'T00:00:00.000Z';
                let dateName = moment(startDate).format('MMMM')
                dateNames.push(dateName)
                console.log("startDate",startDate)
                //let daysNum = moment(startDate).daysInMonth()
                let endDate =moment(startDate).endOf('month').format('YYYY-MM-DD')+ 'T23:59:00.000Z';
                console.log("endDate",endDate)
                let query = {deleted: false,createdAt:{$gte :startDate, $lte : endDate }}
                let ordersCount = await Order.countDocuments(query)
                let ordersQuantity = 0
                await Order.find(query)
                .select('kg').then(async(data)=>{
                    await Promise.all(data.map(async(e)=>{
                        ordersQuantity = ordersQuantity + e.kg
                    }))
                });
                let index = {
                    ordersCount:ordersCount,
                    ordersQuantity:ordersQuantity,
                }
                orders.push(index)
                
            }
            //this quarter
            let startQuarter = moment(date).add(0, 'M').format('YYYY-MM-DD')
            let endQuarter =  moment(date).add(-3, 'M').endOf('month').format('YYYY-MM-DD');
            startQuarter = startQuarter +'T00:00:00.000Z'
            endQuarter = endQuarter +'T23:59:00.000Z'
            let dateName1 = 'Quarter'
            dateNames.push(dateName1)
            //this year
            let startThisYear = moment(date).startOf('year').format('YYYY-MM-DD')
            var endThisYear =  moment(date).endOf('year').format('YYYY-MM-DD');
            startThisYear = startThisYear +'T00:00:00.000Z'
            endThisYear = endThisYear +'T23:59:00.000Z'
            let dateName2 = 'ThisYear'
            dateNames.push(dateName2)
            //last year
            let startLastYear = moment(date).startOf('year').add(-1, 'Y').format('YYYY-MM-DD')
            let endLastYear = moment(startLastYear).endOf('year').format('YYYY-MM-DD')
            startLastYear = startLastYear +'T00:00:00.000Z'
            endLastYear = endLastYear +'T23:59:00.000Z'
            let dateName3 = 'LastYear'
            dateNames.push(dateName3)
            let quarterOrdersCount = await Order.countDocuments({
                deleted: false,
                createdAt:{$gte :startQuarter, $lte : endQuarter }
            })
            let quarterOrdersQuantity = 0
            await Order.find({
                deleted: false,
                createdAt:{$gte :startQuarter, $lte : endQuarter }
            })
            .select('kg').then(async(data)=>{
                await Promise.all(data.map(async(e)=>{
                    quarterOrdersQuantity = quarterOrdersQuantity + e.kg
                }))
            });
            let quarterOrders = {
                ordersCount:quarterOrdersCount,
                ordersQuantity:quarterOrdersQuantity
            }
            orders.push(quarterOrders)
            let thisYearOrdersCount = await Order.countDocuments({
                deleted: false,
                createdAt:{$gte :startThisYear, $lte : endThisYear }
            })
            let thisYearOrdersQuantity = 0
            await Order.find({
                deleted: false,
                createdAt:{$gte :startQuarter, $lte : endQuarter }
            })
            .select('kg').then(async(data)=>{
                await Promise.all(data.map(async(e)=>{
                    thisYearOrdersQuantity = thisYearOrdersQuantity + e.kg
                }))
            });
            let thisYearOrders = {
                ordersCount:thisYearOrdersCount,
                ordersQuantity:thisYearOrdersQuantity
            }
            orders.push(thisYearOrders)
            let lastYearOrdersCount = await Order.countDocuments({
                deleted: false,
                createdAt:{$gte :startLastYear, $lte : endLastYear }
            })
            let lastYearOrdersQuantity=0
            await Order.find({
                deleted: false,
                createdAt:{$gte :startLastYear, $lte : endLastYear }
            })
            .select('kg').then(async(data)=>{
                await Promise.all(data.map(async(e)=>{
                    lastYearOrdersQuantity = lastYearOrdersQuantity + e.kg
                }))
            });
            let lastYearOrders = {
                ordersCount:lastYearOrdersCount,
                ordersQuantity:lastYearOrdersQuantity
            }
            orders.push(lastYearOrders)
            
        }else{
            let dayInMonth =moment(date).daysInMonth();
            let month = moment(date,"YYYY-MM-DD").format("MM")
            for (let i = 1; i <= dayInMonth; i++) {
                let day = i;
                if(i <= 9){
                    day = "0" + i
                }
                
                let startDate = year +"-"+ month +"-"+day + 'T00:00:00.000Z';
                console.log("startDate",startDate)
                //let dateName = moment(startDate).format('MMMM')
                dateNames.push(startDate)
                let endDate = moment(startDate).endOf('month').format('YYYY-MM-DD')+ 'T23:59:00.000Z';
                console.log("endDate",endDate)
                let query = {deleted: false,createdAt:{$gte :startDate, $lte : endDate }}
                let ordersCount = await Order.countDocuments(query)
                let ordersQuantity = 0
                await Order.find(query)
                .select('kg').then(async(data)=>{
                    await Promise.all(data.map(async(e)=>{
                        ordersQuantity = ordersQuantity + e.kg
                    }))
                });
                let index = {
                    ordersCount:ordersCount,
                    ordersQuantity:ordersQuantity,
                }
                orders.push(index)
                
            }
            //this month
            let startThisMonth = moment(date).startOf('month').format('YYYY-MM-DD')
            var endThisMonth =  moment(date).endOf('month').format('YYYY-MM-DD');
            startThisMonth = startThisMonth +'T00:00:00.000Z'
            endThisMonth = endThisMonth +'T23:59:00.000Z'
            let dateName2 = 'ThisMonth'
            dateNames.push(dateName2)
            
            let thisMonthOrdersCount = await Order.countDocuments({
                deleted: false,
                createdAt:{$gte :startThisMonth, $lte : endThisMonth }
            })
            let thisMonthOrdersQuantity = 0
            await Order.find({
                deleted: false,
                createdAt:{$gte :startThisMonth, $lte : endThisMonth }
            })
            .select('kg').then(async(data)=>{
                await Promise.all(data.map(async(e)=>{
                    thisMonthOrdersQuantity = thisMonthOrdersQuantity + e.kg
                }))
            });
            let thisMonthOrders = {
                ordersCount:thisMonthOrdersCount,
                ordersQuantity:thisMonthOrdersQuantity
            }
            orders.push(thisMonthOrders)
        }
        res.send({
            orders:orders,
            dateNames:dateNames
        })
        
    },
    async getOrders(req,res, next) {//done
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,status,city,problem,date,dateType,restaurant} = req.query;
        let query = {deleted: false};
        if(area){
            query.area = area
        } 
        if(city){
            query.city = city
        } 
        if (restaurant) query.restaurant = restaurant;
        if (status) query.status = status;
        if (problem == "true") query.problem = true;
        if (problem =="false") query.problem = false;
        if(status =="COLLECTED"){
            query.status ={$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}
        }
        if(date && dateType){  
            let startDate,endDate
            if(dateType =="DAY"){
                startDate = date + 'T00:00:00.000Z';
                endDate = date + 'T23:59:00.000Z';
                
            }else{
                startDate = moment(date).add(0, 'M').startOf('month').format('YYYY-MM-DD')
                endDate = moment(startDate).format("YYYY-MM-") + moment(startDate).daysInMonth();
                startDate = startDate +'T00:00:00.000Z'
                endDate = endDate +'T23:59:00.000Z'
            }
            Object.assign(query, {"createdAt": {$gte :startDate, $lte : endDate }});
            
        }
        Order.find(query)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .populate(populateQueryOrder).then(async(data)=>{ 
            let newdata = []
            await Promise.all(data.map(async(e)=>{
                let index = {
                    status:e.status,
                    totalPrice:e.totalPrice,
                    requestedKg:e.average,
                    collectedKg:e.kg,
                    warehouseKgAmount:e.warehouseKgAmount,
                    collectedDate:e.collectedDate,
                    problem:e.problem,
                    reason:e.reason,
                    restaurant: {//not use
                        restaurantName_en:e.restaurant.restaurantName,
                        restaurantName_ar:e.restaurant.restaurantName_ar,
                        id:e.restaurant._id
                    },
                    city:{//not use
                        cityName:e.city.cityName,
                        arabicCityName:e.city.arabicCityName,
                        id: e.city._id,
                    },
                    area:{ //not use
                        areaName:e.area.areaName,
                        arabicAreaName:e.area.arabicAreaName,
                        id: e.area._id,
                    },
                    id:e._id
                }
                if(e.didUser){
                    index.didUser ={
                        firstname:e.didUser.firstname,
                        lastname:e.didUser.lastname,
                        username:e.didUser.username,
                        id:e.didUser._id
                    }
                   
                }
                if(e.driver){
                    index.driver ={
                        firstname:e.driver.firstname,
                        lastname:e.driver.lastname,
                        username:e.driver.username,
                        id:e.driver._id
                    }
                   
                }
                
                newdata.push(index)
            }))
            const count = await Order.countDocuments(query);
            const pageCount = Math.ceil(count / limit);
    
            res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
        })
    },







    async restaurantsInfoGraph(req,res, next) {///not used
        
        //active restaurants
        let activeRestaurants1 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let activeRestaurants2 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let activeRestaurants3 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let activeRestaurants4 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let activeRestaurants5 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let activeRestaurants6 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let activeRestaurants7 = await Restaurant.countDocuments({
            deleted: false,
            status:'APPROVED',
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //survey restaurants
        let surveyRestaurants1 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let surveyRestaurants2 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let surveyRestaurants3 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let surveyRestaurants4 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let surveyRestaurants5 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let surveyRestaurants6 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let surveyRestaurants7 = await Restaurant.countDocuments({
            deleted: false,
            status:'SURVEY-ACCOUNT',
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //pruchsing restaurants
        let pruchsingRestaurants1 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let pruchsingRestaurants2 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let pruchsingRestaurants3 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let pruchsingRestaurants4 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let pruchsingRestaurants5 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let pruchsingRestaurants6 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let pruchsingRestaurants7 = await Restaurant.countDocuments({
            deleted: false,
            status:'PURCHASING-ACCOUNT',
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //uncommitted restaurants
        let uncommittedRestaurants1 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let uncommittedRestaurants2 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let uncommittedRestaurants3 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let uncommittedRestaurants4 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let uncommittedRestaurants5 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let uncommittedRestaurants6 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let uncommittedRestaurants7 = await Restaurant.countDocuments({
            deleted: false,
            uncommitted:true,
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        let activeRestaurants = [activeRestaurants1, activeRestaurants2, activeRestaurants3, activeRestaurants4,activeRestaurants5, activeRestaurants6, activeRestaurants7]
        let surveyRestaurants = [surveyRestaurants1, surveyRestaurants2, surveyRestaurants3, surveyRestaurants4, surveyRestaurants5, surveyRestaurants6,surveyRestaurants7]
        let pruchsingRestaurants =[pruchsingRestaurants1, pruchsingRestaurants2,pruchsingRestaurants3, pruchsingRestaurants4, pruchsingRestaurants5, pruchsingRestaurants6,pruchsingRestaurants7]
        let uncommittedRestaurants = [uncommittedRestaurants1,uncommittedRestaurants2,uncommittedRestaurants3,uncommittedRestaurants4,uncommittedRestaurants5,uncommittedRestaurants6,uncommittedRestaurants7]

        res.send({ 
            activeRestaurants,
            surveyRestaurants,
            pruchsingRestaurants,
            uncommittedRestaurants
        })
    },

    async ordersInfoGraph(req,res, next) {///not used
        
        //pending orders
        let pendingorders1 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let pendingorders2 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let pendingorders3 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let pendingorders4 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let pendingorders5 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let pendingorders6 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let pendingorders7 = await Order.countDocuments({
            deleted: false,
            status:'PENDING',
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //collected orders
        let collectedorders1 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let collectedorders2 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let collectedorders3 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let collectedorders4 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let collectedorders5 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let collectedorders6 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let collectedorders7 = await Order.countDocuments({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //canceled orders
        let canceledOrders1 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let canceledOrders2 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let canceledOrders3 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let canceledOrders4 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let canceledOrders5 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let canceledOrders6 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let canceledOrders7 = await Order.countDocuments({
            deleted: false,
            status:'CANCEL',
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        //problem orders
        let problemOrders1 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startDay, $lte : endDay }
        })
        let problemOrders2 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startWeek, $lte : endWeek }
        })
        let problemOrders3 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startMonth, $lte : endMonth }
        })
        let problemOrders4 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        })
        let problemOrders5 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :start6Months, $lte : end6Months }
        })
        let problemOrders6 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        })
        let problemOrders7 = await Order.countDocuments({
            deleted: false,
            problem:true,
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        })
        let pendingorders = [pendingorders1, pendingorders2, pendingorders3, pendingorders4,pendingorders5, pendingorders6, pendingorders7]
        let collectedorders = [collectedorders1, collectedorders2, collectedorders3, collectedorders4, collectedorders5, collectedorders6,collectedorders7]
        let canceledOrders =[collectedorders1, collectedorders2, collectedorders3, collectedorders4, collectedorders5, collectedorders6,collectedorders7]
        let problemOrders = [problemOrders1,problemOrders2,problemOrders3,problemOrders4,problemOrders5,problemOrders6,problemOrders7]
    
        res.send({ 
            pendingorders,
            collectedorders,
            canceledOrders,
            problemOrders
        })
    },
    async salesStatistics(req,res, next) {
        let restaurantsCount = await Restaurant.countDocuments({deleted: false})
        let ordersCount = await Order.countDocuments({deleted: false})
        let totalCollectedPrice = await Order.find({deleted: false,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
        .select('totalPrice')
        totalCollectedPrice = totalCollectedPrice.length>0?totalCollectedPrice.map(o => o.totalPrice).reduce((a, c) => { return a + c }):0
        res.send({
            restaurantsCount,
            ordersCount,
            totalCollectedPrice
        })
    },
    async getRestaurantsSales(req,res, next) {
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,status,uncommitted,city} = req.query;
        let query = {deleted: false};
        if(area){
            Object.assign(query, {"branches.area": area});
        } 
        if(city){
            Object.assign(query, {"branches.city": city});
        } 
        if (status) query.status = status;
        if (uncommitted == "true") query.uncommitted = true;
        if (uncommitted =="false") query.uncommitted = false;
        Restaurant.find(query)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .populate(populateQueryrestaurant).then(async(data)=>{ 
            let newdata = []
            await Promise.all(data.map(async(e)=>{
                let lastCollectedDate = await Order.findOne({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                .sort({collectedDate: -1}).select('createdAt')
                let totalCollected = await Order.find({restaurant:e._id,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                .select('kg')
                totalCollected = totalCollected.length>0?totalCollected.map(o => o.kg).reduce((a, c) => { return a + c }):0
                let index = {
                    restaurantName_en:e.restaurantName,
                    restaurantName_ar:e.restaurantName_ar,
                    status:e.status,
                    uncommitted:e.uncommitted,
                    lastCollectedDate:lastCollectedDate?lastCollectedDate.createdAt:"",
                    totalCollected:totalCollected,
                    id:e._id,
                }
                if(e.purchasing){
                    index.purchasing ={
                        firstname:e.purchasing.firstname,
                        lastname:e.purchasing.lastname,
                        username:e.purchasing.username,
                        id:e.purchasing._id
                    }
                }
                if(e.operation){
                    index.operation ={
                        firstname:e.operation.firstname,
                        lastname:e.operation.lastname,
                        username:e.operation.username,
                        id:e.operation._id
                    }
                }
                /*branches */
                let branches = []
                for (let val of e.branches) {
                    branches.push({
                        city:{ 
                            cityName:val.city.cityName,
                            arabicCityName:val.city.arabicCityName,
                            id: val.city._id,
                        },
                        area:{ 
                            areaName:val.area.areaName,
                            arabicAreaName:val.area.arabicAreaName,
                            id: val.area._id,
                        },
                        destination:val.destination,
                        address:val.address,
                    })
                }
                index.branches = branches;
                newdata.push(index)
            }))
            const count = await Order.countDocuments(query);
            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
        })
    }, 
    async branchInfoGraph(req,res, next) {
        //collected orders
        let collectedOrders1 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startDay, $lte : endDay }
        }).select("kg")
        collectedOrders1 = collectedOrders1.length>0? collectedOrders1.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders2 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startWeek, $lte : endWeek }
        }).select("kg")
        collectedOrders2 = collectedOrders2.length>0? collectedOrders2.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders3 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startMonth, $lte : endMonth }
        }).select("kg")
        collectedOrders3 = collectedOrders3.length>0? collectedOrders3.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders4 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startQuarter, $lte : endQuarter }
        }).select("kg")
        collectedOrders4 = collectedOrders4.length>0? collectedOrders4.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders5 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :start6Months, $lte : end6Months }
        }).select("kg")
        collectedOrders5 = collectedOrders5.length>0? collectedOrders5.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders6 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startThisYear, $lte : endThisYear }
        }).select("kg")
        collectedOrders6 = collectedOrders6.length>0? collectedOrders6.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let collectedOrders7 = await Order.find({
            deleted: false,
            status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']},
            createdAt:{$gte :startLastYear, $lte : endLastYear }
        }).select("kg")
        collectedOrders7 = collectedOrders7.length>0? collectedOrders7.map(o => o.kg).reduce((a, c) => { return a + c }):0
        let orders = [collectedOrders1,collectedOrders2,collectedOrders3,collectedOrders4,collectedOrders5,
            collectedOrders6,collectedOrders7]
        res.send({ 
            orders
        })
    },
    async getBranchs(req,res, next) {
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,city} = req.query;
        let query1 = {deleted: false,type:'DRIVER'}
        let query2 = {deleted: false,type:'OPERATION'}
        let query3 = {deleted: false,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}}
        if(area){
            query1.area = area
            query2.area = area
            query3.area = area
        } 
        if(city){
            query1.city = city
            query2.city = city
            query3.city = city
        } 
        let drivers = await User.find(query1)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .select('firstname lastname username')
        let operators = await User.find(query2)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .select('firstname lastname username')
        let orders = await Order.find(query3)
        let quantity = orders.length>0?orders.map(o => o.kg).reduce((a, c) => { return a + c }):0
        res.send({
            drivers,
            operators,
            quantity
        })
    },
    
    async staffStatistics(req,res, next) {
        let logisticsCount = await User.countDocuments({deleted: false,type:'OPERATION'})
        let salesCount = await User.countDocuments({deleted: false,type:{$in:['PURCHASING','SURVEY']}})
        let driversCount = await User.countDocuments({deleted: false,type:'DRIVER'})
        let warehouseUsersCount = await User.countDocuments({deleted: false,type:'WAREHOUSE'})
        res.send({
            logisticsCount,
            salesCount,
            driversCount,
            warehouseUsersCount
        })
    },
    async getStaffUsers(req,res, next) {
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,type,city} = req.query;
        let query = {deleted: false};
        if(area){
            query.area = area
        } 
        if(city){
            query.city = city
        } 
        if(type){
            query.type = type
        } 
        User.find(query)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .then(async(data)=>{ 
            let newdata = []
            await Promise.all(data.map(async(e)=>{
                console.log(e)
                let index = {
                    firstname:e.firstname,
                    lastname:e.lastname,
                    username:e.username,
                    title:e.title,
                    salary:e.salary?e.salary:"",
                    contract:e.contract?e.contract:"",
                    id:e._id
                }
               
                newdata.push(index)
            }))
            const count = await User.countDocuments(query);
            const pageCount = Math.ceil(count / limit);
    
            res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
        })
    },
    async surveyMonthlyGraph(req,res, next) {
        try {
            let {userId} = req.params
            let restaurants1 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants2 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth2, $lte : endMonth2 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants3 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth3, $lte : endMonth3 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants4 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth4, $lte : endMonth4 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants5 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth5, $lte : endMonth5 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants6 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth6, $lte : endMonth6 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants7 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth7, $lte : endMonth7 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants8 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth8, $lte : endMonth8 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants9 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth9, $lte : endMonth9 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants10 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth10, $lte : endMonth10 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants11 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth11, $lte : endMonth11 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants12 = await Restaurant.countDocuments({
                deleted: false,
                survey:userId,
                createdAt:{$gte :startMonth12, $lte : endMonth12 }
            })
            let restaurants = [restaurants1,restaurants2,restaurants3,restaurants4,restaurants5,restaurants6,restaurants7,restaurants8,restaurants9,restaurants10,restaurants11,restaurants12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                restaurants,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async purchasingMonthlyGraph(req,res, next) {
        try {
            let {userId} = req.params
            let restaurants1 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth1, $lte : endMonth1 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants2 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth2, $lte : endMonth2 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants3 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth3, $lte : endMonth3 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants4 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth4, $lte : endMonth4 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants5 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth5, $lte : endMonth5 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants6 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth6, $lte : endMonth6 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants7 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth7, $lte : endMonth7 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants8 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth8, $lte : endMonth8 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants9 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth9, $lte : endMonth9 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants10 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth10, $lte : endMonth10 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants11 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth11, $lte : endMonth11 }
            })
            ////////////////////////////////////////////////////////////////////
            let restaurants12 = await Restaurant.countDocuments({
                deleted: false,
                purchasing:userId,
                createdAt:{$gte :startMonth12, $lte : endMonth12 }
            })
            let restaurants = [restaurants1,restaurants2,restaurants3,restaurants4,restaurants5,restaurants6,restaurants7,restaurants8,restaurants9,restaurants10,restaurants11,restaurants12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                restaurants,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    
    async driverMonthlyGraph(req,res, next) {
        try {
            let {userId} = req.params
            let orders1 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth1) , $lte : Date.parse(endMonth1) }
            }).select('kg')
            console.log(orders1)
            orders1 =orders1.length>0? orders1.map(o => o.kg).reduce((a, c) => { return a + c }):0
           console.log(orders1)
            ////////////////////////////////////////////////////////////////////
            let orders2 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth2) , $lte : Date.parse(endMonth2) }
            }).select('kg')
            console.log(orders2)
            orders2 =orders2.length>0? orders2.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders3 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth3) , $lte : Date.parse(endMonth3) }
            }).select('kg')
            orders3 =orders3.length>0? orders3.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders4 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth4) , $lte : Date.parse(endMonth4) }
            }).select('kg')
            orders4 =orders4.length>0? orders4.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders5 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth5) , $lte : Date.parse(endMonth5) }
            }).select('kg')
            orders5 =orders5.length>0? orders5.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders6 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth6) , $lte : Date.parse(endMonth6) }
            }).select('kg')
            orders6 =orders6.length>0? orders6.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders7 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth7) , $lte : Date.parse(endMonth7) }
            }).select('kg')
            orders7 =orders7.length>0? orders7.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders8 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth8) , $lte : Date.parse(endMonth8) }
            }).select('kg')
            orders8 =orders8.length>0? orders8.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders9 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth9) , $lte : Date.parse(endMonth9) }
            }).select('kg')
            orders9 =orders9.length>0? orders9.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders10 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth10) , $lte : Date.parse(endMonth10) }
            }).select('kg')
            orders10 = orders10.length>0?orders10.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders11 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth11) , $lte : Date.parse(endMonth11) }
            }).select('kg')
            orders11 = orders11.length>0?orders11.map(o => o.kg).reduce((a, c) => { return a + c }):0
            ////////////////////////////////////////////////////////////////////
            let orders12 = await Order.find({
                deleted: false,
                status:{$nin:["CANCEL"]},
                driver:userId,
                createdDate:{$gte :Date.parse( startMonth12) , $lte : Date.parse(endMonth12) }
            }).select('kg')
            orders12 = orders12.length>0?orders12.map(o => o.kg).reduce((a, c) => { return a + c }):0
            let orders = [orders1,orders2,orders3,orders4,orders5,orders6,orders7,orders8,orders9,orders10,orders11,orders12];
            let months = [startM1,startM2,startM3,startM4,startM5,startM6,startM7,startM8,startM9,startM10,startM11,startM12]
            res.send({
                orders,
                months
            })
        } catch (err) {
            next(err);
        }
    },
    async warehouseStatistics(req,res, next) {
        let warehouseCount = await Warehouse.countDocuments({deleted: false})
        let warehouseUsersCount = await User.countDocuments({deleted: false,type:'WAREHOUSE'})
        //quantity out from warehouses
        let shipping = await Warehouse.find({
            deleted: false,
        }).select('exportOli')
        shipping = shipping.length>0?shipping.map(o => o.exportOli).reduce((a, c) => { return a + c }):0
        //quantity enter to warehouses
        let quantity = await Warehouse.find({
            deleted: false,
            
        }).select('receiveOli')
        quantity = quantity.length>0?quantity.map(o => o.receiveOli).reduce((a, c) => { return a + c }):0
        res.send({
            quantity,
            shipping,
            warehouseCount,
            warehouseUsersCount
        })
    },
    async getWarehouse(req,res, next) {
        let page = +req.query.page || 1, limit = +req.query.limit || 20
        ,{area,type,city} = req.query;
        let query = {deleted: false};
        if(area){
            query.area = area
        } 
        if(city){
            query.city = city
        } 
        Warehouse.find(query)
        .limit(limit)
        .skip((page - 1) * limit).sort({ _id: -1 })
        .then(async(data)=>{ 
            let newdata = []
            await Promise.all(data.map(async(e)=>{
                let allWareHouseShipping = await Shipping.find({warehouse:e._id,deleted: false})
                let airportWeight = allWareHouseShipping.length >0 ?allWareHouseShipping.map(o => o.firstMeasure).reduce((a, c) => { return a + c }):0
                let clientWeight = allWareHouseShipping.length >0 ?allWareHouseShipping.map(o => o.secondMeasure).reduce((a, c) => { return a + c }):0
                /*let transCode = [];
                allWareHouseShipping.forEach(v => {
                    transCode.push(v.vehicleNumber)
                });*/
                let orders = await Order.find({warehouse:e._id,deleted: false,status:{$in:['COLLECTED','ARRIVED','STORED','EXPORT-TO-TANK']}})
                let orderWeight = orders.length >0 ?orders.map(o => o.kg).reduce((a, c) => { return a + c }):0
                
                let index = {
                    name:e.name,
                    receiveOli:e.receiveOli,
                    exportOli:e.exportOli,
                    //transCode:transCode,
                    orderWeight:orderWeight,
                    airportWeight:airportWeight,
                    clientWeight:clientWeight,
                    id:e._id
                }
               
                newdata.push(index)
            }))
            const count = await Warehouse.countDocuments(query);
            const pageCount = Math.ceil(count / limit);
    
            res.send(new ApiResponse(newdata, page, pageCount, limit, count, req));
        })
    },


    
}