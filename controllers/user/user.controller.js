import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Attend from "../../models/attend/attend.model";

import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import bcrypt from 'bcryptjs';
import config from '../../config';
import { sendForgetPassword } from '../../services/message-service';
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import { sendEmail } from "../../services/emailMessage.service";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}
const populateQuery = [
    { path: 'restaurant', model: 'restaurant' },
    { path: 'warehouse', model: 'warehouse' },
];
const populateQuery2 = [
    { path: 'user', model: 'user' },
];
export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let user = req.user;
            user = await User.findById(user.id).populate(populateQuery);
            if(!user)
                return next(new ApiError(403, ('password incorrect')));
            if(user.deleted == true){
                    return next(new ApiError(403, ('this user is deleted')));
                }
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            if(user){
                if(req.body.location){
                    user.location = req.body.location;
                }
            }
            
            res.status(200).send({
                user,
                token: generateToken(user.id)
            });
            
            /*let reports = {
                "action":"User Login",
            };

            let report = await Report.create({...reports, user: user });*/
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            
            body('firstname').optional(),
            body('lastname').optional(),
            body('language').optional(),
            body('phone').optional(),
            body('location').optional(),
            body('job').optional(),
            body('title').optional(),
            body('warehouse').optional(),
            body('username').not().isEmpty().withMessage('username is required')
            .custom(async (value, { req }) => {
                let userQuery = { username: value };
                if (isUpdate && req.user.username === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('username duplicated'));
                else
                    return true;
            }),
            body('email').optional()
                /*.isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                })*/,

                body('type').not().isEmpty().withMessage('type is required')
                .isIn( ['DRIVER', 'ADMIN','USER','OPERATION','PURCHASING','SURVEY','WAREHOUSE','CALL-CENTER']).withMessage('wrong type'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').optional()
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            /*let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });
*/
        } catch (err) {
            next(err);
        }
    },
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = true;
            await activeUser.save();
            /*let reports = {
                "action":"block User",
            };
            let  report = await Report.create({...reports, user: user });*/
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = false;
            await activeUser.save();
            /*let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });*/
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = true;
            await activeUser.save();
            /*let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });*/
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'Qsat active your account'
            });
            let notif = {
                "description":'Qsat active your account',
                "arabicDescription":'تم تفعيل الحساب الخاص بك'
            }
            await Notif.create({...notif,resource:req.user,target:userId,subject:activeUser.id});
            res.send('user active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            /*let reports = {
                "action":"Dis-Active User",
            };
            let report = await Report.create({...reports, user: user });*/
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send({user});
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            /*let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });*/
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            /*let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });*/
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },


    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            user.verifycode = generateVerifyCode(); 
            await user.save();
            //send code
            let text = user.verifycode.toString();
            let description = 'souq elteb verfication code';
            sendEmail(validatedBody.email, text,description)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
           
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },

    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type, active,warehouse} = req.query;
            let query = {deleted: false };
            if (type) query.type = type;
            if (active) query.active = active;
            if (warehouse) query.warehouse = warehouse;
            let users = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutPagenation(req, res, next) {
        try {
            let{type, active,warehouse} = req.query;
            let query = {deleted: false };
            if (type) query.type = type;
            if (active) query.active = active;
            if (warehouse) query.warehouse = warehouse;
            let users = await User.find(query).sort({ createdAt: -1 })
            res.send(users)
        } catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let {userId } = req.params;
            if (req.user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            user.deleted = true
            await user.save();
            /*let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user._id });*/
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async tracking(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            user.location = req.body.location
            await user.save();
           
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async start(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            user.startDate = new Date();
            user.end = false
            await user.save();
            let attend = await Attend.create({user:userId,statDate:Date.parse(new Date()),start:new Date()});
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async end(req, res, next) {
        try {
            let {userId } = req.params;
            let user = await checkExistThenGet(userId, User,{deleted: false });
            user.endDate = new Date();
            user.end = true;
            await user.save();
            let attend = await Attend.findOne({user:userId,leave:false});
            console.log(attend)
            //let attendd = await checkExistThenGet(attend.id, Attend,{deleted: false });
           
            attend.end = new Date();
            attend.endDate = Date.parse(new Date());
            attend.leave = true;
            attend.save();
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async findAttend(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {user,today,start,end} = req.query;
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            let query = {deleted: false };
            if(today){
                query = {
                    $and: [
                        {statDate: { $gte : from, $lte :to}},
                        {deleted: false } ,
                    ]
                };

            }
            if(start && end){
                let startDate = start + 'T00:00:00.000Z';
                let endDate = end + 'T23:59:00.000Z';
                console.log(startDate);
                console.log(endDate);
                let from = Date.parse(startDate);
                let to = Date.parse(endDate);
                console.log(from);
                console.log(to);
                query = {
                    $and: [
                        {statDate: { $gte : from, $lte :to}},
                        {deleted: false }
                    ]
                };
            }
            
            if (user) query.user = user;
            let attends = await Attend.find(query).populate(populateQuery2)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            const attendsCount = await Attend.count(query);
            const pageCount = Math.ceil(attendsCount / limit);
            res.send(new ApiResponse(attends, page, pageCount, limit, attendsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateUpdatedUser(isUpdate = true) {
        let validation = [
            body('firstname').optional(),
            body('lastname').optional(),
            body('language').optional(),
            body('job').optional(),
            body('title').optional(),
            body('warehouse').optional(),
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('email').optional()
              /*  .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            })*/,
            body('username').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { username: value };
                    if (isUpdate && user.username === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('username duplicated'));
                    else
                        return true;
            }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn( ['DRIVER', 'ADMIN','USER','OPERATION','PURCHASING','SURVEY','WAREHOUSE','CALL-CENTER']).withMessage('wrong type'),

        ];
        if (isUpdate)
            validation.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validation;
    },
    async updateAdminInfo(req, res, next) {
        try {
           
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if (req.body.password) {
                 user.password = req.body.password;
            }
        
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.language){
                user.language = validatedBody.language;
            }
        
            if(validatedBody.firstname){
                user.firstname = validatedBody.firstname;
            }
            if(validatedBody.lastname){
                user.lastname = validatedBody.lastname;
            }
            if(validatedBody.username){
                user.username = validatedBody.username;
            }
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
        
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
            if(validatedBody.type){
                user.type = validatedBody.type;
            }
            if(validatedBody.job){
                user.job = validatedBody.job;
            }
            if(validatedBody.title){
                user.title = validatedBody.title;
            }
            if(validatedBody.warehouse){
                user.warehouse = validatedBody.warehouse;
            }
        
           
            
            await user.save();
           
            res.status(200).send({
                user: await User.findOne(user).populate(populateQuery),
            });


        } catch (error) {
            next(error);
        }
    },

    async updateUserInfo(req, res, next) {
        try {
           
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            if (bcrypt.compareSync(req.body.password, user.password)) {
                    
                if (req.file) {
                    let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                    validatedBody.img = image;
                }
                if (req.body.password) {
                     user.password = req.body.password;
                }
            
                if(validatedBody.phone){
                    user.phone = validatedBody.phone;
                }
                if(validatedBody.language){
                    user.language = validatedBody.language;
                }
            
                if(validatedBody.firstname){
                    user.firstname = validatedBody.firstname;
                }
                if(validatedBody.lastname){
                    user.lastname = validatedBody.lastname;
                }
                if(validatedBody.username){
                    user.username = validatedBody.username;
                }
                if(validatedBody.email){
                    user.email = validatedBody.email;
                }
            
                if(validatedBody.img){
                    user.img = validatedBody.img;
                }
                if(validatedBody.type){
                    user.type = validatedBody.type;
                }
                if(validatedBody.job){
                    user.job = validatedBody.job;
                }
                if(validatedBody.title){
                    user.title = validatedBody.title;
                }
                if(validatedBody.warehouse){
                    user.warehouse = validatedBody.warehouse;
                }
            
               
            }
            else {
                res.status(400).send({
                    error: [
                        {
                           
                            msg: 'password is incorrect'
                        }
                    ]
                });
            }
        
          
            
            await user.save();
            /*let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user });*/
            res.status(200).send({
                user: await User.findOne(user).populate(populateQuery),
            });


        } catch (error) {
            next(error);
        }
    },



};
