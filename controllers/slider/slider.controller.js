import Slider from "../../models/slider/slider.model";
import Report from "../../models/reports/report.model";

import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExistThenGet } from "../../helpers/CheckMethods";
import { handleImgs,handleImg, checkValidations } from "../shared/shared.controller";
import { toImgUrl } from "../../utils";

export default {
    validateBody(isUpdate = false) {
        let validations = [
            body('title').optional(),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));

            const validatedBody = checkValidations(req);
            if(req.files){
                let images = await handleImgs(req);
                validatedBody.img = images
            }
            let slider = await Slider.create({ ...validatedBody });
            return res.status(201).send({slider});
        } catch (error) {
            next(error);
        }
    },

    async getAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let sliders = await Slider.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Slider.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(sliders, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        } 
    }, 
    async findById(req, res, next) {
        try {
            let { SliderId  } = req.params;
            res.send(
                await checkExistThenGet( SliderId ,  Slider, { deleted: false})
            );
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { SliderId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
            }
            let updatedSlider = await Slider.findByIdAndUpdate(SliderId, {
                ...validatedBody
            });
            /*let reports = {
                "action":"Update slider",
            };
            let report = await Report.create({...reports, user: user });
            */
            return res.status(200).send(updatedSlider);
        } catch (error) {
            next(error);
        }
    },
    async updateSlider(req, res, next) {
        try {
            let user = req.user;
            let { SliderId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let slider = await checkExistThenGet(SliderId, Slider, { deleted: false });
            slider.img = req.body.images;
            console.log(req.body.images)
            await slider.save();
            res.status(200).send(slider);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { SliderId } = req.params;

        try {
            let slider = await checkExistThenGet(SliderId, Slider, { deleted: false });
            
            slider.deleted = true;
            await slider.save();
            /*let reports = {
                "action":"delete slider",
            };
            let report = await Report.create({...reports, user: user });
            */
            res.send('deleted');

        } catch (err) {
            next(err);
        }
    },
    async deleteImg(req, res, next) {
        try {
            
            let {SliderId } = req.params;
            let slider = await checkExistThenGet(SliderId, Slider, { deleted: false });
            let data = [];
            data = slider.img;
            console.log(data);
            let index = req.body.index;
           // data.unshift(data.splice(index, 1)[0]);
            data.splice(index,1)
            console.log(data)
            slider.img = data;
            await slider.save();
            res.status(200).send(slider);
        }
        catch (err) {
            next(err);
        }
    },
    async addImg(req, res, next) {
        try {
            
            let {SliderId } = req.params;
            let slider = await checkExistThenGet(SliderId, Slider, { deleted: false });
            let data = [];
            data = slider.img;
           let image
           if(req.file){
               image = await handleImg(req);
           }
            data.push(image)
            console.log(data)
            slider.img = data;
            await slider.save();
            res.status(200).send(slider);
        }
        catch (err) {
            next(err);
        }
    },
}