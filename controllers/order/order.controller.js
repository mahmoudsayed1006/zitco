import { checkExist,checkCouponExist, checkExistThenGet, isLng, isLat} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Order from "../../models/order/order.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import { ValidationError } from "mongoose";
import { handleImg, checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model"
import moment from 'moment';
import Restaurant from "../../models/resturant/resturant.model";
import Warehouse from "../../models/warehouse/warehouse.model";

const populateQuery = [
    { path: 'driver', model: 'user' },
    { path: 'restaurant', model: 'restaurant' },
    { path: 'wareHouse', model: 'warehouse' },
    {
        path: 'restaurant.branches', model: 'restaurant',
        populate: { path: 'city', model: 'city'}
    },
    {
        path: 'restaurant.branches', model: 'restaurant',
        populate: { path: 'area', model: 'area'}
    },
    
];

var OrderController = {
    async findOrders(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{didUser, driver,restaurant,asign,asignToWareHouse,status,finish,all,wareHouse,problem,start,type} = req.query;
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);

        if(start){
            if(start =="true") {
                let query = {
                    $and: [
                        {dateInMillesec: { $gte : from, $lte :to}},
                        {deleted: false } ,
                        {problem:false}
                    ]
                };
                if(all){
                    query ={deleted: false}
                }
                if(problem == "true"){
                    query.problem = true
                }
                if(problem == "false"){
                    query.problem = false
                }
                if (driver)
                    query.driver = driver;
                if (type)
                    query.type = type;
                if (wareHouse)
                    query.wareHouse = wareHouse;
                if (restaurant)
                    query.restaurant = restaurant;
                if (status)
                    query.status = status;
                if (didUser)
                    query.didUser = didUser;
                
                if (asign=="true") {
                    query.asign = true;
                } 
                if (asign=="false") {
                    query.asign = false;
                } 
                if (asignToWareHouse=='true') {
                    query.asignToWareHouse = true;
                } 
                if (asignToWareHouse=='false') {
                    query.asignToWareHouse = false;
                } 
                if (finish=="true") {
                    query.finish = true;
                } 
                if (finish=="false") {
                    query.finish = false;
                } 
                
                let orders = await Order.find(query).populate(populateQuery)
                    .sort({ actionDate: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit);

                orders.forEach(async (order) =>  {
                    let theOrder = await checkExistThenGet(order._id, Order);
                    /*function stringToDate(_date,_format,_delimiter)
                    {
                        var formatLowerCase=_format.toLowerCase();
                        var formatItems=formatLowerCase.split(_delimiter);
                        var dateItems=_date.split(_delimiter) ;
                        var monthIndex=formatItems.indexOf("mm");
                        var dayIndex=formatItems.indexOf("dd");
                        var yearIndex=formatItems.indexOf("yyyy");
                        var month=parseInt(dateItems[monthIndex]);
                        month-=1;
                        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
                        return formatedDate;
                    }
                    let d = stringToDate(order.date,"dd-mm-yyyy","-")
                    let theDate  = moment(d).format()
                    console.log(theDate)
                    */
                    theOrder.dateInMillesec = Date.parse(order.date)
                    console.log()
                    await theOrder.save();
                });
    
                orders = await Order.find(query).populate(populateQuery)
                .sort({ actionDate: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
                const ordersCount = await Order.count(query);
                const pageCount = Math.ceil(ordersCount / limit);
    
                res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
            } if(start =="false") {
                return next(new ApiError(500, ("you can't show your order please click start")));
            }
        } else{
            let query = {
                $and: [
                    {dateInMillesec: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {problem:false}
                ]
            };
            console.log(Date.parse('2020-07-07T23:17:03.566Z'))
            if(all){
                query ={deleted: false}
            }
            if (driver)
                query.driver = driver;
               
            if (wareHouse)
                query.wareHouse = wareHouse;
            if (restaurant)
                query.restaurant = restaurant;
            if (status)
                query.status = status;
            if (didUser)
                query.didUser = didUser;
            if (asign=="true") {
                query.asign = true;
            } 
            if (asign=="false") {
                query.asign = false;
            } 
            
            if (asignToWareHouse=='true') {
                query.asignToWareHouse = true;
            } 
            if (asignToWareHouse=='false') {
                query.asignToWareHouse = false;
            } 
            if (finish=="true") {
                query.finish = true;
            } 
            if (finish=="false") {
                query.finish = false;
            } 
            if(problem == "true"){
                query.problem = true
            }
            if(problem == "false"){
                query.problem = false
            }
            if (type)
                    query.type = type;
            let orders = await Order.find(query).populate(populateQuery)
            .sort({ actionDate: -1 })
            .limit(limit)
            .skip((page - 1) * limit);
            orders.forEach(async (order) =>  {
                let theOrder = await checkExistThenGet(order._id, Order);
                theOrder.dateInMillesec = Date.parse(order.date)
                await theOrder.save();
            });
            orders = await Order.find(query).populate(populateQuery)
            .sort({ actionDate: -1 })
            .limit(limit)
            .skip((page - 1) * limit);
            const ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);
            res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        }
        
        
            
        } catch (err) {
            next(err);
        }
    },
    async countOrder(req,res, next) {
        try {
            let {driver,status,all,problem} = req.query
            var mydate = new Date().toISOString().slice(0, 10);           
            let start = mydate + 'T00:00:00.000Z';
            let end= mydate + 'T23:59:00.000Z';
            console.log(start);
            console.log(end);
            let from = Date.parse(start);
            let to = Date.parse(end);
            console.log(from);
            console.log(to);
            let query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {problem: false } ,
                ]
            };
            if(problem == "true"){
                query.problem = true
            }
            if(problem == "false"){
                query.problem = false
            }
            console.log(Date.parse('2020-07-07T23:17:03.566Z'))
            if(all){
                query ={deleted: false}
            }
            if (driver)
                query.driver = driver;
            if (status)
                query.status = status;
            const orderCount = await Order.count(query);
            
            res.status(200).send({
                order:orderCount   
            });
        } catch (err) {
            next(err);
        }
        
    },
    
    async addOrder(socket,data,nsp){
        var userId = data.userId
        var toUserRoom ="room-" +  data.userId 
        var orderData = { //شكل الداتا 
            restaurant: data.restaurant,
            average: data.average,
            date: data.date,
            branch:data.branch,
            createdDate:Date.parse(new Date())
        }
        if(data.date != null){
            orderData.dateInMillesec = Date.parse(data.date)
        }
        //outlet area
        let restaurant = await checkExistThenGet(data.restaurant, Restaurant);
        restaurant.lastOrder = Date.parse(new Date())
        let area = restaurant.branches[data.branch].area;
       
        if(area != null){
            orderData.area = area
        }
        let user = await checkExistThenGet(data.userId, User);
        if(user.type == "USER"){
            orderData.type = "OTOMATIC";
        }
        orderData.didUser = data.userId;
        
        //outlet serial
        const orderCount = await Order.count({restaurant:data.restaurant});
        let count = orderCount + 1;
        console.log("count = " + count)
        let serialCode = restaurant.outletCode + "-" + count
        console.log("serialCode = " + serialCode)
        if(serialCode != null){
            orderData.serialCode = serialCode
        }
        await restaurant.save();
        var order = new Order(orderData);    
        let query = {
            $and: [
                { $or: [
                    {type: 'ADMIN'},   
                    {type: 'OPERATION'},     
                  ] 
                },
                {deleted: false} 
            ]
        };
        
        order.save()
        .then(async (data1) => {
            /*let reports = {
                "action":"Create New Order",
            };
          
            let report = Report.create({...reports, user: data.userId,order:data1._id  });*/
            let users = await User.find(query);
            console.log(users)
            var query1 = {
                _id: data1._id
            };
            Order.find(query1).populate(populateQuery)
                .then(async (data2) => {
                    nsp.to(toUserRoom).emit('callCenterOrder', {order:data2});
                });
            users.forEach(user => {
                var toRoom = 'room-' + user.id; 
                console.log(user.id)
                Order.find(query1).populate(populateQuery)
                .then(async (data) => {
                    nsp.to(toRoom).emit('newOrder', {order:data});
                });
               // nsp.to(toRoom).emit('newOrder', {data:data1});
               sendNotifiAndPushNotifi({////////
                targetUser: user.id, 
                fromUser: data.userId, 
                text: 'new notification',
                subject: data1._id,
                subjectType: 'new order'
            });
            let notif = {
                "description":'new order',
                "arabicDescription":"طلب جديد"
            }
            Notif.create({...notif,resource:data.userId,target:user.id,order:data1._id});
            
            })
           
        })
        .catch((err)=>{
            console.log(err)
        });
    },
    async updateOrder(nsp, data,socket) { 
        var userId = data.userId
        var orderId = data.orderId ;
        var query1 = {
            _id: orderId
        };
        /*let reports = {
            "action":"update order",
        };
        let report = await Report.create({...reports, user: userId,order:orderId });*/
        Order.updateMany(query1,{restaurant:data.restaurant,average:data.average,price:data.price,date:data.date})
            .exec()
            .then(async(result) => {
                Order.find(query1).populate(populateQuery)
                .then(async (data) => {
                    socket.emit('updateOrder', {order: data});       
                });
            })
            .catch((err) => {
                console.log(err);
            });
    },
    async update(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            if(req.body.restaurant){
                order.restaurant = req.body.restaurant;
            }
            if(req.body.average){
                order.average = req.body.average;
            }
            if(req.body.date){
                order.date = req.body.date;
            }
            if(req.body.priceForKg){
                order.priceForKg = req.body.priceForKg;
            }
            if(req.body.totalPrice){
                order.totalPrice = req.body.totalPrice;
            }
            if(req.body.kg){
                order.kg = req.body.kg;
            }
          
            await order.save();
           /* let reports = {
                "action":"update order",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.send({order});
            
        } catch (error) {
            next(error);
        }
    },
    async assignOrder(nsp, data,socket) {  //asign to driver
        var userId = data.userId
        var orderId = data.orderId ;
        var driver = data.driverId
        var query1 = {
            _id: orderId
        };
        /*let reports = {
            "action":"Assigned order to driver",
        };
        let report = await Report.create({...reports, user: userId,order:orderId });*/
        let driverr = await checkExistThenGet(data.driverId, User);
        Order.updateMany(query1, {status:'ASSIGNED',asign:true,wareHouse:driverr.warehouse, driver:driver,actionDate :new Date(),assignToDriverDate: new Date(),})
            .exec()
            .then(async(result) => {
                var toRoom = 'room-' + driver; 
                Order.find(query1).populate(populateQuery)
                .then(async (data) => {
                    nsp.to(toRoom).emit('assignOrder', {order:data});
                });
                sendNotifiAndPushNotifi({////////
                    targetUser: driver, 
                    fromUser:  userId, 
                    text: 'new notification',
                    subject: orderId,
                    subjectType: 'new order'
                });
                let notif = {
                    "description":'new order',
                    "arabicDescription":'طلب جديد'
                }
                Notif.create({...notif,resource: userId,target:driver,order:orderId});
            })
            .catch((err) => {
                console.log(err);
            });
    },
    async assignOrderToWarehouse(socket, data,nsp) {  //asign to warehouse
        var userId = data.userId
        var orderId = data.orderId ;
        var warehouse = data.warehouseId;
        console.log(warehouse)
        var query1 = {
            _id: orderId
        };
        var query = { 
            warehouse: data.warehouseId
        };
        /*let reports = {
            "action":"Assigned order to warehouse",
        };
        let report = await Report.create({...reports, user: userId,order:orderId });*/
        Order.updateMany(query1, {asignToWareHouse:true, wareHouse:data.warehouseId,actionDate :new Date(),assignToWarehouseDate: new Date(),})
            .exec()
            .then(async(result) => {
                let users = await User.find(query);
                console.log(users)
                users.forEach(user => {
                    var toRoom = 'room-' + user.id; 
                    console.log(user.id)
                    Order.find(query1).populate(populateQuery)
                    .then(async (data) => {
                        nsp.to(toRoom).emit('assignOrderToWarehouse', {order:data});
                    });
                    sendNotifiAndPushNotifi({////////
                        targetUser: user.id, 
                        fromUser:  userId, 
                        text: 'new notification',
                        subject: orderId,
                        subjectType: 'new order'
                    });
                    let notif = {
                        "description":'new order',
                        "arabicDescription":'طلب جديد'
                    }
                    Notif.create({...notif,resource: userId,target:user.id,order:orderId});
                })
                
            })
            .catch((err) => {
                console.log(err);
            });
    },

    async findById(req, res, next) {
        try {
            let {orderId } = req.params;
            res.send(
                await checkExistThenGet(orderId, Order, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    async cancel(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'CANCEL';
            order.cancelDate = new Date();
            order.actionDate =new Date();
            await order.save();
            let query = {
                $and: [
                    { $or: [
                        {type: 'ADMIN'},   
                        {type: 'OPERATION'},     
                      ] 
                    },
                    {deleted: false} 
                ]
            };
            
            let users = await User.find(query);
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: orderId,
                    subjectType: 'cancel order'
                });
                let notif = {
                    "description":'cancel order',
                    "arabicDescription":'تم الغاء الطلب'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,order:orderId});
            });
           
            if(order.driver){
                sendNotifiAndPushNotifi({////////
                    targetUser: order.driver, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: orderId,
                    subjectType: 'cancel order'
                });
                let notif = {
                    "description":'cancel order',
                    "arabicDescription":'تم الغاء الطلب'
                }
                Notif.create({...notif,resource:req.user._id,target:order.driver,order:orderId});
            
            }
            /*let reports = {
                "action":"User cancel order",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.send('User cancel order');
            
        } catch (error) {
            next(error);
        }
    },
    async accept(req, res, next) { //driver take task
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'ONPROGRESS';
            order.driverTakeDate = new Date();
            order.actionDate =new Date();
            await order.save();
            /*let reports = {
                "action":"driver accept order",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.send('driver accept order');
            
        } catch (error) {
            next(error);
        }
    },
    async collect(req, res, next) { //driver take oli
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'COLLECTED';
            order.collectedDate = new Date();
            order.actionDate =new Date();
            await order.save();
            /*let reports = {
                "action":"order collected",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.send('order collected');
            
        } catch (error) {
            next(error);
        }
    },
    async arrive(req, res, next) {//driver arrive to warehouse
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'ARRIVED';
            order.driverArriveDate = new Date();
            await order.save();
            /*let reports = {
                "action":"driver arrived to warehouse",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.send('driver arrived to warehouse');
            
        } catch (error) {
            next(error);
        }
    },
    async stored(req, res, next) {//warehouse stored oli
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.status = 'STORED';
            order.finish = true;
            order.warehouseUser = req.user._id;
            order.warehouseKgAmount = req.body.warehouseKgAmount;
            order.warehouseReceiveDate = new Date();
            await order.save();
            let warehouse = await checkExistThenGet(order.wareHouse, Warehouse, { deleted: false });
            warehouse.freeCapacity = warehouse.freeCapacity - req.body.warehouseKgAmount;
            warehouse.receiveOli = warehouse.receiveOli + req.body.warehouseKgAmount;
            warehouse.oilQuantity = warehouse.oilQuantity + req.body.warehouseKgAmount;
            let reports = {
                "action":"warehouse stored oli",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId,type:'finish' });
            res.send('warehouse stored oli');
            
        } catch (error) {
            next(error);
        }
    },
    async problem(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.problem = true;
            order.reason = req.body.reason;
            await order.save();
            let reports = {
                "action":"driver have a problem",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId,type:'problem' });
            res.send('driver have a problem');
            
        } catch (error) {
            next(error);
        }
    },
    
    async delete(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            order.deleted = true;
            await order.save();
            /*let reports = {
                "action":"Delete Order",
            };
            let report = await Report.create({...reports, user: req.user,order:orderId });*/
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async getOrder(req,res, next) {
        try {
        let page = +req.query.page || 1, limit = +req.query.limit || 20
            ,{status,daily,weekly,monthly,start,end,driver,area} = req.query
        let query = {deleted: false};
        if(start && end){
            let startDate = start + 'T00:00:00.000Z';
            let endDate = end + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(daily){
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(weekly){
            let d = new Date();
            var to = Date.parse(new Date());
            var from = d.setDate(d.getDate() - 7);
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(monthly){ //
            console.log(new Date())
            let d = new Date().toISOString().slice(0, 8);
            console.log(d)
            var to = Date.parse(new Date());
            var from = d + '01T00:00:00.000Z';
            let fromMilleSec = Date.parse(from)
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : fromMilleSec, $lte :to}},
                    {deleted: false }
                ]
            };
        }
       
        if (status) query.status = status;
        if (driver) query.driver = driver;
        if (area) query.area = area;
        let ordersCount = await Order.count(query) //
        let orders = await Order.find(query).populate(populateQuery)
        .sort({ createdAt: -1 })
        .limit(limit)
        .skip((page - 1) * limit); //get orders
        
        const pageCount = Math.ceil(ordersCount / limit);

        res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        
        } catch (err) {
            next(err);
        }
        
    },
    async getOliAverageAndCount(req,res, next) {//driver /// area
        try {
        let {status,daily,weekly,monthly,start,end,driver,area} = req.query
        let query = {deleted: false};
        if(start && end){
            let startDate = start + 'T00:00:00.000Z';
            let endDate = end + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(daily){
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(weekly){
            let d = new Date();
            var to = Date.parse(new Date());
            var from = d.setDate(d.getDate() - 7);
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false }
                ]
            };
        }
        if(monthly){ //
            console.log(new Date())
            let d = new Date().toISOString().slice(0, 8);
            console.log(d)
            var to = Date.parse(new Date());
            var from = d + '01T00:00:00.000Z';
            let fromMilleSec = Date.parse(from)
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : fromMilleSec, $lte :to}},
                    {deleted: false }
                ]
            };
        }
       
        if (status) query.status = status;
        if (driver) query.driver = driver;
        if (area) query.area = area;
        let ordersCount = await Order.count(query) //
        let orders = await Order.find(query);
        var total = 0;

        for (var i = 0; i < orders.length; i++) { 
            total += orders[i].kg
        }

        let average = total / ordersCount;
        res.status(200).send({
            total:total,
            average:average
        });
        } catch (err) {
            next(err);
        }
        
    },
    
   
};
module.exports = OrderController;