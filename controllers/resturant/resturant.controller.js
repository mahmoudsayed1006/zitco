import { checkExist, checkExistThenGet, isLng, isLat, isArray, isNumeric } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Restaurant from "../../models/resturant/resturant.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import { ValidationError } from "mongoose";
import { checkValidations,handleImg ,distance} from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { sendEmail } from "../../services/emailMessage.service";
import Area from "../../models/area/area.model";
import Notif from "../../models/notif/notif.model"
const populateQuery = [
    { path: 'director', model: 'user' },
    { path: 'purchasing', model: 'user' },
    { path: 'proceed', model: 'user' },
    { path: 'operation', model: 'user' },
    { path: 'survey', model: 'user' },
    { path: 'branches.city', model: 'city' },
    { path: 'branches.area', model: 'area' },
    { path: 'owner', model: 'user' },
];
export default {
    async findrestaurants(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{area,orderByArea,search,withoutPagenation,noOrderEventUser,noOrder, status,decline,all,purchasing,survey,operation,noOil,Agreement,uncommitted,lat,lang,sortByKm} = req.query
                , query = {deleted: false,decline:false};
            if(all){
                query ={deleted: false}
            } 
            if(search && all) {
                query = {
                    $and: [
                        { $or: [
                            {restaurantName: { $regex: '.*' + search + '.*' }}, 
                            {restaurantName_ar: { $regex: '.*' + search + '.*' }}, 
                            ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if(search) {
                query = {
                    $and: [
                        { $or: [
                            {restaurantName: { $regex: '.*' + search + '.*' }}, 
                            {restaurantName_ar: { $regex: '.*' + search + '.*' }}, 
                            ] 
                        },
                        {deleted: false},
                        {decline: false},
                    ]
                };
            }
            if(area){
                Object.assign(query, {"branches.area": area});
            } 
            if (noOrder=="true") {
                query.noOrder = true;
            } 
            if (noOrder=="false") {
                query.noOrder = false;
            } 
            if (status) query.status = status;
            if (noOrderEventUser) query.noOrderEventUser = noOrderEventUser;
            if (decline == "true") query.decline = true;
            if (decline =="false") query.decline = false;
            if (noOil == "true") query.noOil = true;
            if (noOil =="false") query.noOil = false;
           
            if (uncommitted == "true") query.uncommitted = true;
            if (uncommitted =="false") query.uncommitted = false;
            if(purchasing) query.purchasing = purchasing
            if(survey) query.survey = survey
            if(operation) query.operation = operation;
            if(Agreement) query.Agreement = Agreement;
            let sortt = { createdAt: -1 }
            if(sortByKm){
                sortt = {distance:-1}
            }
            let restaurants 
            if(withoutPagenation){
                restaurants = await Restaurant.find(query).populate(populateQuery).sort(sortt)
            }else{
                restaurants = await Restaurant.find(query).populate(populateQuery)
                .sort(sortt)
                .limit(limit)
                .skip((page - 1) * limit);
            }
            
            restaurants.forEach(async (restaurant) =>  {
                let theRestaurant = await checkExistThenGet(restaurant._id, Restaurant);
                let destination = theRestaurant.branches[0].destination;
                if(lat && lang){
                    let numberOfKm = distance(destination[0],destination[1],lat,lang,"K")
                    theRestaurant.distance = numberOfKm;
                }
                var date1 = new Date(); 
                var date2 = new Date(theRestaurant.lastOrder); 
                var Difference_In_Time = date2.getTime() - date1.getTime(); 
                var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                if(Difference_In_Days > 30 ){
                    theRestaurant.uncommitted = true;
                }
                await theRestaurant.save();

            });
            if(withoutPagenation){
                restaurants = await Restaurant.find(query)
                    .populate(populateQuery)
                    .sort(sortt)
            }else{
                restaurants = await Restaurant.find(query).populate(populateQuery)
                    .sort(sortt)
                    .limit(limit)
                    .skip((page - 1) * limit);
                
                
            }
            let finalData = []
            if(orderByArea){
                let areas = await Area.find({deleted: false });
                for (let area of areas) {
                    for (let restaurant of restaurants) {
                        let branches = restaurant.branches;
                        for (let branche of branches) {
                            if(branche.area._id == area._id){
                                finalData.push(restaurant) 
                            }
                        }
                    }
                }
              
                restaurants = finalData
            }
            if(withoutPagenation){
                res.send(restaurants)
            }else{
                const restaurantsCount = await Restaurant.count(query);
                const pageCount = Math.ceil(restaurantsCount / limit);
                res.send(new ApiResponse(restaurants, page, pageCount, limit, restaurantsCount, req));
            }
        } catch (err) {
            next(err);
        }
    },
    async countAccount(req,res, next) {
        try {
        let { status,decline,all} = req.query
            , query = {deleted: false,decline:false };
        if(all){
            query ={deleted: false}
        }
        if (status) query.status = status;
        if (decline == "true") query.decline = true;
        if (decline =="false") query.decline = false;
            const accountCount = await Restaurant.count(query);
            
            res.status(200).send({
                accountCount:accountCount   
            });
        } catch (err) {
            next(err);
        }
        
    },
    async getAccountCountBySurvey(req,res, next) {
        try {
        let {surveyId} = req.params;
        let {status,daily,weekly,monthly,start,end} = req.query
        let query = {deleted: false,survey:surveyId};
        if(start && end){
            let from = Date.parse(start);
            let to = Date.parse(end);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {survey:surveyId}
                ]
            };
        }
        if(daily){
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {survey:surveyId}
                ]
            };
        }
        if(weekly){
            let d = new Date();
            var to = Date.parse(new Date());
            var from = d.setDate(d.getDate() - 7);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {survey:surveyId}
                ]
            };
        }
        if(monthly){ //
            console.log(new Date())
            let d = new Date().toISOString().slice(0, 8);
            console.log(d)
            var to = Date.parse(new Date());
            var from = d + '01T00:00:00.000Z';
            let fromMilleSec = Date.parse(from)
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : fromMilleSec, $lte :to}},
                    {deleted: false } ,
                    {survey:surveyId}
                ]
            };
        }
        if (status) query.status = status;
        const accountCount = await Restaurant.count(query);
        res.status(200).send({
            accountCount:accountCount   
        });
        } catch (err) {
            next(err);
        }
        
    },
    async getAccountCountByPurchasing(req,res, next) {
        try {
        let {purchasingId} = req.params;
        let {status,daily,weekly,monthly,start,end} = req.query
        let query = {deleted: false,purchasing:purchasingId};
        if(start && end){
            let from = Date.parse(start);
            let to = Date.parse(end);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {purchasing:purchasingId}
                ]
            };
        }
        if(daily){
            var mydate = new Date().toISOString().slice(0, 10);           
            let startDate = mydate + 'T00:00:00.000Z';
            let endDate= mydate + 'T23:59:00.000Z';
            console.log(startDate);
            console.log(endDate);
            let from = Date.parse(startDate);
            let to = Date.parse(endDate);
            console.log(from);
            console.log(to);
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {purchasing:purchasingId}
                ]
            };
        }
        if(weekly){
            let d = new Date();
            var to = Date.parse(new Date());
            var from = d.setDate(d.getDate() - 7);
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : from, $lte :to}},
                    {deleted: false } ,
                    {purchasing:purchasingId}
                ]
            };
        }
        if(monthly){ //
            console.log(new Date())
            let d = new Date().toISOString().slice(0, 8);
            console.log(d)
            var to = Date.parse(new Date());
            var from = d + '01T00:00:00.000Z';
            let fromMilleSec = Date.parse(from)
            console.log(from)
            console.log(to)
            query = {
                $and: [
                    {createdDate: { $gte : fromMilleSec, $lte :to}},
                    {deleted: false } ,
                    {purchasing:purchasingId}
                ]
            };
        }
       
        if (status) query.status = status;
        const accountCount = await Restaurant.count(query);
        res.status(200).send({
            accountCount:accountCount   
        });
        } catch (err) {
            next(err);
        }
        
    },
    validateCreatedrestaurants(isUpdate = false) {
        let validations = [
            body('restaurantName').not().isEmpty().withMessage('restaurant Name is required'),
            body('restaurantName_ar').not().isEmpty().withMessage('restaurant Name arabic is required'),
            body('title').optional(),
            body('note').optional(),

            body('outletPhoneNumber').optional(),
            body('average').not().isEmpty().withMessage('average is required'),
            body('status').not().isEmpty().withMessage('status is required'),
            body('price').optional(),
            body('space').not().isEmpty().withMessage('space is required'),
            body('oilType').not().isEmpty().withMessage('oilType is required'),
            body('payType').not().isEmpty().withMessage('payType is required'),
            body('address').optional(),
            body('noOil').optional(),
            body('branchesNumber').not().isEmpty().withMessage('branches Number is required'),
            body('firstname').optional(),
            body('lastname').optional(),
            body('phone').optional(),
            body('price2').optional(),
            body('username').optional()
            .custom(async (value, { req }) => {
                let userQuery = { username: value };
                if (isUpdate && req.user.username === value)
                    userQuery._id = { $ne: req.user._id };

                if (await User.findOne(userQuery))
                    throw new Error(req.__('username duplicated'));
                else
                    return true;
            }),
            body('email').optional()
                .isEmail().withMessage('email syntax')
               /* .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                })*/,
            body('branches').custom(vals => isArray(vals)).withMessage('branches should be an array')
                .isLength({ min: 1 }).withMessage('branches should have at least one element of branches')
                .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('destination').optional(),
                        body('area').not().isEmpty().withMessage('area is required'),
                        body('city').optional(),
                        body('address').optional()
                    }
                    return true;
                }),
            body('type').optional()
                .isIn( ['DRIVER', 'ADMIN','USER','OPERATION','PURCHASING','SURVEY']).withMessage('wrong type'),
            body('class').optional()
                .isIn( ['CLASS-A', 'CLASS-B','CLASS-C']).withMessage('wrong type'),
            
            body('password').optional(),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },
            
    async create(req, res, next) {
        try {
            
            await checkExist(req.user._id, User);
            let user = req.user;
            if (user.active != true)
            return next(new ApiError(403, ('blocked'))); 
            const validatedBody = checkValidations(req);
            if(validatedBody.noOil == "true"){
                validatedBody.noOil = true
            }
            if(validatedBody.noOil == "false"){
                validatedBody.noOil = false
            }
            validatedBody.createdDate = Date.parse(new Date());
            if(req.file){
                let image = await handleImg(req);
                validatedBody.img = image
            }
            if (user.type == 'SURVEY'){
                validatedBody.status ='SURVEY-ACCOUNT',
                validatedBody.surveyDate = Date.parse(new Date())
                validatedBody.survey = req.user._id

            }
            if (user.type == 'PURCHASING' )
                validatedBody.status = 'PROCEED'
           
            let createdRestaurant = await Restaurant.create({ ...validatedBody,director: req.user});
            
            if (user.type == 'ADMIN' || user.type == 'OPERATION'){
                let createdUser = await User.create({
                    firstname:validatedBody.firstname,
                    lastname:validatedBody.lastname,
                    username:validatedBody.username,
                    email:validatedBody.email,
                    phone:validatedBody.phone,
                    type:'USER',
                    password:validatedBody.password,
                    class:validatedBody.class,
                    restaurant:createdRestaurant.id
    
                });
                let restaurantt = await checkExistThenGet(createdRestaurant.id, Restaurant);
                restaurantt.owner = createdUser.id;
                validatedBody.status ='APPROVED';
                validatedBody.approvedDate = Date.parse(new Date())
                restaurant.approvedBy = req.user._id
               
                ////////////////class //////////////////////////
                if(validatedBody.class == "CLASS-A"){
                    let text = "username : " + validatedBody.username +"<br></br> password : " +validatedBody.password
                    let description = 'account information in zeitco';
                    sendEmail(validatedBody.email, text,description)
                }
                if (await Restaurant.findOne({deleted:false,status:'APPROVED'})){
                    let lastResturant = await Restaurant.findOne({deleted:false,status:'APPROVED'})
                    let newSerial = parseInt(lastResturant.outletApprovedSerial) + 1;
                    restaurantt.outletApprovedSerial = newSerial;
                }
                
                await restaurantt.save();

            }
            console.log()
           
            let restaurant = await Restaurant.populate(createdRestaurant, populateQuery);
          
            
            /*let reports = {
                "action":"Create New restaurant",
            };
            let report = await Report.create({...reports, user: req.user });*/
            let users = await User.find({'type':'PURCHASING'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: createdRestaurant.id,
                    subjectType: 'new restaurant'
                });
                let notif = {
                    "description":'new restaurant',
                    "arabicDescription":'تم تسجيل مطعم جديد'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,restaurant:createdRestaurant.id});
            });
           
            res.status(201).send(restaurant);
        } catch (err) {
            next(err);
            
        }
    },
    async signUp(req, res, next) {
        try {
            
            const validatedBody = checkValidations(req);
            let userQuery = { username: validatedBody.username };
            if (await User.findOne(userQuery))
            return next(new ApiError(422, ('username duplicated')));
            validatedBody.createdDate = Date.parse(new Date());
            if(req.file){
                let image = await handleImg(req);
                validatedBody.img = image
            }
            let createdRestaurant = await Restaurant.create({ ...validatedBody});
            let restaurant = await Restaurant.populate(createdRestaurant, populateQuery);

            res.status(201).send(restaurant);
        } catch (err) {
            next(err);
            
        }
    },

    async findById(req, res, next) {
        try {
            let {ResturantId } = req.params;
            res.send(
                await checkExistThenGet(ResturantId, Restaurant, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    validateUpdaterestaurants(isUpdate = false) {
        let validations = [
            body('restaurantName').not().isEmpty().withMessage('restaurant Name is required'),
            body('outletPhoneNumber').optional(),
            body('title').optional(),
            body('note').optional(),
            body('average').not().isEmpty().withMessage('average is required'),
            body('status').not().isEmpty().withMessage('status is required'),
            body('price').optional(),
            body('space').not().isEmpty().withMessage('space is required'),
            body('oilType').not().isEmpty().withMessage('oilType is required'),
            body('payType').not().isEmpty().withMessage('payType is required'),
            body('address').optional(),
            body('branchesNumber').not().isEmpty().withMessage('branches Number is required'),
            body('firstname').optional(),
            body('lastname').optional(),
            body('phone').optional(),
            body('email').optional(),
            body('username').optional(),
            body('price2').optional(),
            body('noOil').optional(),
            body('branches').custom(vals => isArray(vals)).withMessage('branches should be an array')
                .isLength({ min: 1 }).withMessage('branches should have at least one element of branches')
                .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('destination').optional(),
                        body('area').not().isEmpty().withMessage('area is required'),
                        body('city').optional(),
                        body('address').optional()
                    }
                    return true;
                }),
            body('type').optional()
                .isIn( ['DRIVER', 'ADMIN','USER','OPERATION','PURCHASING','SURVEY']).withMessage('wrong type'),
            body('class').optional()
                .isIn( ['CLASS-A', 'CLASS-B','CLASS-C']).withMessage('wrong type'),
            
            body('password').optional(),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { ResturantId } = req.params;

            const validatedBody = checkValidations(req);
            if(validatedBody.noOil == "true"){
                validatedBody.noOil = true
            }
            if(validatedBody.noOil == "false"){
                validatedBody.noOil = false
            }
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedResturant = await Restaurant.findByIdAndUpdate(ResturantId, {
                ...validatedBody
            });
            if(user.type =='ADMIN' || user.type == 'OPERATION'){
                let restaurant = await checkExistThenGet(ResturantId, Restaurant);
                if (restaurant.status != 'APPROVED'){
                    let createdUser = await User.create({
                        firstname:validatedBody.firstname,
                        lastname:validatedBody.lastname,
                        username:validatedBody.username,
                        email:validatedBody.email,
                        phone:validatedBody.phone,
                        type:'USER',
                        password:validatedBody.password,
                        restaurant:ResturantId
        
                    });
                    console.log(createdUser.id);
                    restaurant.owner = createdUser.id;
                    if(validatedBody.class == "CLASS-A"){
                        let text = "username : " + validatedBody.username +"<br></br> password : " +validatedBody.password
                        let description = 'account information in zeitco';
                        sendEmail(validatedBody.email, text,description)
                    }
                    if (await Restaurant.findOne({deleted:false,status:'APPROVED'})){
                        let lastResturant = await Restaurant.findOne({deleted:false,status:'APPROVED'})
                        let newSerial = lastResturant.outletApprovedSerial + 1;
                        restaurant.outletApprovedSerial = newSerial;
                    }
                }
                console.log(restaurant.owner);
                restaurant.operation = req.user._id;
                await restaurant.save();
            }
            /*let reports = {
                "action":"Update Resturant",
            };
            let report = await Report.create({...reports, user: user });
            */
            return res.status(200).send(updatedResturant);
        } catch (error) {
            next(error);
        }
    },
    async PURCHASING(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.status = 'PURCHASING-ACCOUNT';
            restaurant.purchasing = req.user._id;
            await restaurant.save();
            /*let reports = {
                "action":"assign restaurant to Purchasing Team",
            };
            let report = await Report.create({...reports, user: req.user });
            */
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async PURCHASINGChosen(req, res, next) {
        try {
            let { ResturantId,userId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.status = 'PURCHASING-ACCOUNT';
            restaurant.purchasing = userId;
            if(req.body.reassign == "true"){
                restaurant.reassign = true
            }
            restaurant.Agreement = 'PENDING'
            await restaurant.save();
            /*let reports = {
                "action":"assign restaurant to Purchasing Team",
            };
            let report = await Report.create({...reports, user: req.user });
            */
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async PROCEED(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.status = 'PROCEED';
            restaurant.agreement = true;
            restaurant.proceed = req.user._id;
            await restaurant.save();
            /*let reports = {
                "action":"PROCEED restaurant",
            };
            let report = await Report.create({...reports, user: req.user });*/
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async APPROVED(req, res, next) {
        try {
            let { ResturantId } = req.params;
            
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);

            if (restaurant.status != 'APPROVED' && restaurant.owner == null){
                let createdUser = await User.create({
                    firstname:restaurant.firstname,
                    lastname:restaurant.lastname,
                    username:restaurant.username,
                    email:restaurant.email,
                    phone:restaurant.phone,
                    type:'USER',
                    password:restaurant.username,
                    restaurant:ResturantId
    
                });
                console.log(createdUser.id);
                restaurant.owner = createdUser.id;
            }
            console.log(restaurant.owner);
            restaurant.status = 'APPROVED';
            restaurant.approvedBy = req.user._id
            restaurant.approvedDate = Date.parse(new Date())
            restaurant.operation = req.user._id;
            if(req.body.class){
                restaurant.class = req.body.class;
            } 
            await restaurant.save();
           /* let reports = {
                "action":"APPROVED restaurant",
            };
            let report = await Report.create({...reports, user: req.user });*/
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
   
    async delete(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.deleted = true;
            await restaurant.save();
            /*let reports = {
                "action":"Delete restaurant",
            };
            let report = await Report.create({...reports, user: req.user });*/
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async ignore(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.decline = true;
            if(req.body.declineReason)
                restaurant.declineReason= req.body.declineReason;
            await restaurant.save();
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async removeIgnore(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.decline = false;
            restaurant.declineReason ="";
            await restaurant.save();
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async nonAgreement(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.Agreement = 'NON-AGREEMENT';
            restaurant.agrementActionBy = req.user._id
            if(req.body.nonAgreementReason)
                restaurant.nonAgreementReason= req.body.nonAgreementReason;
            await restaurant.save();
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async agreement(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.Agreement = "AGREEMENT";
            restaurant.agreementDate = Date.parse(new Date())
            restaurant.agrementActionBy = req.user._id
            restaurant.nonAgreementReason ="";
            await restaurant.save();
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async noOrder(req, res, next) {
        try {
            let { ResturantId } = req.params;
            let restaurant = await checkExistThenGet(ResturantId, Restaurant);
           
            restaurant.noOrder = true;
            restaurant.noOrderEventUser = req.user._id
            if(req.body.reason)
                restaurant.noOrderReason= req.body.reason;
            await restaurant.save();
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    
    
}