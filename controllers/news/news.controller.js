import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import News from "../../models/news/news.model";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let news = await News.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const NewssCount = await News.count(query);
            const pageCount = Math.ceil(NewssCount / limit);

            res.send(new ApiResponse(news, page, pageCount, limit, NewssCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let news = await News.find(query)
                .sort({ createdAt: -1 });
            res.send(news)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('title').not().isEmpty().withMessage('title is required'),
            body('description').not().isEmpty().withMessage('description is required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            let image;
            if(req.file){
                image = await handleImg(req);
            }
            
            console.log(image);
             console.log(image);
            let createdNews = await News.create({ ...validatedBody,img:image});

            /*let reports = {
                "action":"Create News",
            };
            let report = await Report.create({...reports, user: user });
            */
            res.status(201).send(createdNews);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { newsId } = req.params;
            await checkExist(newsId, News, { deleted: false });
            let news = await News.findById(newsId);
            res.send(news);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { newsId } = req.params;
            await checkExist(newsId, News, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedNews = await News.findByIdAndUpdate(newsId, {
                ...validatedBody,
            }, { new: true });
            /*let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });*/
            res.status(200).send(updatedNews);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { newsId } = req.params;
            let news = await checkExistThenGet(newsId, News, { deleted: false });
            
            news.deleted = true;
            await news.save();
            /*let reports = {
                "action":"Delete News",
            };
            let report = await Report.create({...reports, user: user });*/
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};