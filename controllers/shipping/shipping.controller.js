import Shipping from "../../models/shipping/shipping.model";
import Area from "../../models/area/area.model";
import User from "../../models/user/user.model";
import Warehouse from "../../models/warehouse/warehouse.model";

import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";
const populateQuery = [
    { path: 'director', model: 'user' },
    { path: 'warehouse', model: 'warehouse' },
    
];
export default {
    validateshippingBody(isUpdate = false) {
        return [
            body('shippingDay').not().isEmpty().withMessage('shipping day Required'),
            body('tankNumber').not().isEmpty().withMessage('tank Number Required'),
            body('waybill').not().isEmpty().withMessage('waybill Required'),
            body('driverName').not().isEmpty().withMessage('driverName Required'),
            body('vehicleNumber').not().isEmpty().withMessage('vehicleNumber Required'),
            body('amount').not().isEmpty().withMessage('amount Required'),
            body('director').optional(),
            body('secondMeasure').optional(),
            body('firstMeasure').optional(),
            body('warehouse').not().isEmpty().withMessage('warehouse Required')
            .isNumeric().withMessage('warehouse should be number')
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.warehouse, Warehouse);
            let shipping = await Shipping.create({ ...validatedBody });
            /*let reports = {
                "action":"Create shipping",
            };
            let report = await Report.create({...reports, user: user });*/
            return res.status(201).send(shipping);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
            let user = req.user;

            let page = +req.query.page || 1, limit = +req.query.limit || 20
            ,{ done,warehouse,director,accept} = req.query;
            let query = { deleted: false }
            if (done=="true") {
                query.done = true; 
            } 
            if (done=="false") {
                query.done = false;
            } 
            if (accept=="true") {
                query.accept = true;
            } 
            if (accept=="false") {
                query.accept = false;
            } 
            if (warehouse) {
                query.warehouse = warehouse;
            } 
            if (director) {
                query.director = director;
            } 
            let shipping = await Shipping.find(query).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit)
                .sort({ _id: -1 });

            let count = await Shipping.count(query);

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(shipping, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { shippingId } = req.params;
            const validatedBody = checkValidations(req);
            let shipping = await Shipping.findByIdAndUpdate(shippingId, { ...validatedBody });
            /*let reports = {
                "action":"Update shipping",
            };
            let report = await Report.create({...reports, user: user });*/
            return res.status(200).send(shipping);
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { shippingId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            return res.send(shipping);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { shippingId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            
            shipping.deleted = true;
            await shipping.save();
            /*let reports = {
                "action":"Delete shipping",
            };
            let report = await Report.create({...reports, user: user });*/
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },
    async firstMeasure(req, res, next) {//aireportMeasure
        let { shippingId} = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            
            shipping.firstMeasure = req.body.firstMeasure;
            await shipping.save();
            
            res.send();

        } catch (err) {
            next(err);
        }
    },
    async secondMeasure(req, res, next) {//secondMeasure
        let { shippingId} = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            
            shipping.secondMeasure = req.body.secondMeasure;
            await shipping.save();
            
            res.send();

        } catch (err) {
            next(err);
        }
    },
    async accept(req, res, next) {
        let { shippingId} = req.params;

        try {
            let user = req.user;
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            shipping.director = req.user._id;
            shipping.acceptDate = new Date();
            shipping.accept = true;
           
            res.send(shipping);

        } catch (err) {
            next(err);
        }
    },
    async done(req, res, next) {
        let { shippingId} = req.params;

        try {
            let user = req.user;
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            shipping.amount = req.body.amount;
            shipping.doneDate = new Date();
            shipping.done = true;
            await shipping.save();
            let warehouse = await checkExistThenGet(shipping.warehouse, Warehouse, { deleted: false });
            warehouse.freeCapacity = warehouse.freeCapacity + shipping.capacity;
            warehouse.exportOli = warehouse.exportOli + shipping.amount;
            /*let reports = {
                "action":"shipping done",
            };
            let report = await Report.create({...reports, user: user });*/
            res.send(shipping);

        } catch (err) {
            next(err);
        }
    }
}