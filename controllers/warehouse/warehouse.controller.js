import Warehouse from "../../models/warehouse/warehouse.model";
import Area from "../../models/area/area.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import Order from "../../models/order/order.model";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";

export default {
    validatewarehouseBody(isUpdate = false) {
        return [
            body('name').not().isEmpty().withMessage('name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { name: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.warehouseId };
                    if (await Warehouse.findOne(userQuery))
                        throw new Error('warehouse Name duplicated');
                    else
                        return true;
                }),
            body('oilQuantity').not().isEmpty().withMessage('oilQuantity Required'),
            body('capacity').optional(),            
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            validatedBody.freeCapacity = validatedBody.capacity
            console.log(validatedBody)
            let warehouse = await Warehouse.create({ ...validatedBody });
            /*let reports = {
                "action":"Create warehouse",
            };
            let report = await Report.create({...reports, user: user });*/
            return res.status(201).send(warehouse);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
            let warehouses = await Warehouse.find({ deleted: false });
            return res.status(200).send(warehouses);
        } catch (error) {
            next(error);
        }
    },
    async availableWarehouse(req, res, next) {
        try { 
            let {orderId} = req.params;
            let order = await checkExistThenGet(orderId, Order);
            let query = {
                $and: [
                    {capacity: { $gte : parseInt(order.average)}},
                    {deleted: false } ,
                ]
            };
            let warehouses = await Warehouse.find(query);
            return res.status(200).send(warehouses);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let user = req.user;
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let warehouses = await Warehouse.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Warehouse.count({ deleted: false });
            console.log(warehouses)
            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(warehouses, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { warehouseId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let warehouse = await Warehouse.findByIdAndUpdate(warehouseId, { ...validatedBody });
            /*let reports = {
                "action":"Update warehouse",
            };
            let report = await Report.create({...reports, user: user });*/
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { warehouseId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let warehouse = await checkExistThenGet(warehouseId, Warehouse, { deleted: false });
            return res.send(warehouse);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { warehouseId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let warehouse = await checkExistThenGet(warehouseId, Warehouse, { deleted: false });
            let users = await User.find({ warehouse: warehouseId });
            for (let user of users) {
                user.deleted = true;
                await user.save();
            }
            warehouse.deleted = true;
            await warehouse.save();
            /*let reports = {
                "action":"Delete warehouse",
            };
            let report = await Report.create({...reports, user: user });*/
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },
    async outOil(req, res, next) {
        let { warehouseId} = req.params;

        try {
            let user = req.user;
            let warehouse = await checkExistThenGet(warehouseId, Warehouse, { deleted: false });
            warehouse.oilQuantity = warehouse.oilQuantity - req.body.kgAmount;
            await warehouse.save();
            
            res.send();

        } catch (err) {
            next(err);
        }
    }
}