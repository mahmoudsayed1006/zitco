
var OrderController = require('../controllers/order/order.controller');

module.exports = {

    startChat: function (io) {  
        console.log('socket is on')
        
        var nsp = io.of('/order');

        nsp.on('connection', (socket) => { 
            socket.emit('announcements', { message: 'A new user has joined!' });
               nsp.emit('hi', 'Hello everyone!'); 
            var myId = socket.handshake.query.id;
           
            var roomName = 'room-' + myId; 
            socket.join(roomName); 
            console.log('client ' + myId + ' connected.');

            var clients1 = nsp.clients(); 
            socket.userId = myId; 
            console.log("socket: "+socket.userId);
            var clients=[];
            for (var id in clients1.connected) { 
               
                var userid= clients1.connected[id].userId;
                clients.push(userid);
            }
           
            socket.on('newOrder', function (data) { 
                console.log(data);
                OrderController.addOrder(socket,data,nsp);
            });
            socket.on('assignOrder', function (data) { 
                console.log(data);
                OrderController.assignOrder(socket,data,nsp);
            });
            socket.on('assignOrderToWarehouse', function (data) { 
                console.log(data);
                OrderController.assignOrderToWarehouse(socket,data,nsp);
            });
            socket.on('updateOrder', function (data) { 
                console.log(data);
                OrderController.updateOrder(socket,data,nsp);
            });
          
        });
    },
    startNotification : function(io){
        global.notificationNSP = io.of('/notification') ; 
        notificationNSP.on('connection',function(socket){
            var id = socket.handshake.query.id;
            var roomName = 'room-' + id;
            socket.join(roomName);
            console.log('client ' + id + ' connected on notification .');
        });
    }
}